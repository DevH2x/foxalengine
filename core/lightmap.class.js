
class Lighting {
    /**
     * Create light controller
     * @param {HTMLCanvasElement} solidmask - need to casting
     */
	constructor(solidmask){
		this.solidmask = solidmask;
		this.lights = [];
		this.render_quality = 1;
		this.canvas = document.createElement('canvas');
		this.ambient = "#000";
		this.diffusion = 8;
		this.diffusion_samples = 32;
		this.enabled = true;
	}

    /**
     * Add point light to rendering
     * @param {*} x 
     * @param {*} y 
     * @param {*} r 
     * @param {*} c 
     */
	addLight(x, y, r, c){
		x+=8;
		this.lights.push({
			x, y, r, color: "#" + c.toString(16)
		})

		return this.lights[this.lights.length - 1];
	}

    /**
     * Set ambient color
     * @param {*} hex 
     */
	setAmbient(hex){
		this.ambient = "#" + hex.toString(16);
	}

    /**
     * Render lightmap to texture
     */
	render(){
		let {canvas} = this;
		let c = canvas.getContext('2d');
		let s = this.solidmask.getContext('2d').getImageData(0,0,this.solidmask.width, this.solidmask.height);
		let rays = 360 * (this.render_quality || 1)
		let ray_angle = (Math.PI * 2) / rays;

		let collide = function(x, y){
			x = Math.round(x);
			y = Math.round(y);
			return s.data[y * this.solidmask.width * 4 + x * 4];
		}.bind(this);

		let ray = function(x, y, dir, r){

			let xx = x
			let yy = y
			for(let i = 0; i < r; i++){
				xx = x + Math.cos(dir) * i;
				yy = y + Math.sin(dir) * i;
				let col = collide(xx, yy)
				if(col === 255){
					return {x:xx, y:yy}
				}
			}
			return {x: xx, y: yy};
		}

		canvas.width = this.solidmask.width;
		canvas.height = this.solidmask.height;

		c.fillStyle = this.ambient;
		c.fillRect(0,0,canvas.width, canvas.height);

		for(let j = 0; j < this.lights.length; j++){
			let light = this.lights[j];
			let first = ray(light.x, light.y, 0, light.r);
			c.beginPath();
			c.moveTo(first.x, first.y);
			for(let i = 0; i < rays; i++){
				let point = ray(light.x, light.y, ray_angle * i, light.r);
				c.lineTo(point.x, point.y)
			}
			c.fillStyle = c.createRadialGradient(light.x, light.y, 0, light.x, light.y, light.r);
			c.fillStyle.addColorStop(1, '#000000');
			c.fillStyle.addColorStop(0, light.color);
			c.strokeStyle = c.fillStyle;
			c.globalCompositeOperation = 'lighten';
			c.fill();
			c.stroke();

			c.globalCompositeOperation = 'source-over';
		}

		let samples = 32;
		let angle = 360 / this.diffusion_samples;
		angle = angle / 180 * Math.PI;

		c.globalAlpha = 0.1;
		for(let i = 0; i < this.diffusion_samples; i++){
			c.drawImage(canvas, Math.cos(angle * i) * this.diffusion, Math.sin(angle * i) * this.diffusion);
		}
		c.globalAlpha = 1;
	}

    /**
     * Draw lightmap to context
     * @param {*} context 
     * @param {*} x 
     * @param {*} y 
     * @param {*} samples 
     */
	drawLightmap(context, x, y, samples, camera){
		if (this.enabled) {
            context.globalCompositeOperation = 'multiply';
			context.drawImage(this.canvas, 	camera.x, camera.y, context.canvas.width, context.canvas.height, 
											camera.x, camera.y, context.canvas.width, context.canvas.height);
            context.globalCompositeOperation = 'overlay';
			
			for (let i = 0; i < samples; i++) {
				context.drawImage(this.canvas, 	camera.x, camera.y, context.canvas.width, context.canvas.height, 
												camera.x, camera.y, context.canvas.width, context.canvas.height);
            }
			
			context.globalCompositeOperation = 'source-over';
        } else {
            context.globalCompositeOperation = 'multiply';
            context.fillStyle = this.ambient;
            for (let i = 0; i < samples; i++) {
                context.fillRect(0, 0, this.canvas.width, this.canvas.height);
            }
            context.globalCompositeOperation = 'source-over';
		}
	}
}

export default Lighting;