
class TileMap {

    /**
     * Load tilemap file
     * @param {string} file - link to file
     * @param {function} callback - calls when map is loaded
     */
	constructor(file, callback){
		this.data = null;
		fetch(file, {mode:'no-cors'})
			.then(e=>{
				return e.json();
			})
			.then(data=>{
				let tilescount = data.tilesets.length;
				let tilesloaded = 0;
				for(let i = 0; i < data.tilesets.length; i++){
					let e = data.tilesets[i];
					this.createTileset(e, tileset=>{
						e.rendering = tileset;
						tilesloaded++;

						if(tilesloaded === tilescount){
							this.data = data;
							this.data.objects = this.data.layers.filter(e=>{
								return e.type === 'objectgroup';
							})
							this.prerender();
							if(callback) callback();
						}
					})
				}

			})
			.catch(error=>{
				console.error(error);
			})
    }
    
    /**
     * Prerender tilemap to texture
     */
	prerender(){
		let {data} = this;


		//Rendering Layers
		let tilelayers = data.layers.filter(e=>{
			return e.type === 'tilelayer';
		})


		for(let i = 0; i < tilelayers.length;i++){
			let layer = tilelayers[i];
			let width = layer.width;
			layer.canvas = document.createElement('canvas');
			layer.canvas.width = data.width * data.tilewidth;
			layer.canvas.height = data.height * data.tileheight;
			layer.context = layer.canvas.getContext('2d');

            if (layer.properties) {
                let result = {}
                layer.properties.forEach(prop => {
                    result[prop.name] = prop.value
                })

                layer.properties = result
            }

			let c = layer.context;
			for (let j = 0; j < layer.data.length; j++){
				let x = j % width;
				let y = Math.floor(j / width);
				this.drawTile(layer.data[j], x, y, c);
			}
		}
	}


    /**
     * Prerender layer to solid mask
     * @param {string} layername 
     */
	prerenderSolidMask(layername){
		if(!this.data) return false;
		let {data} = this;
		let canvas = document.createElement('canvas');
		let context = canvas.getContext('2d');
		let layer = data.layers.find(e=>{
			return e.name === layername;
		})


		canvas.width = layer.canvas.width;
		canvas.height = layer.canvas.height;

		let layerdata = layer.context.getImageData(0, 0, canvas.width, canvas.height).data;
		let solid = context.getImageData(0, 0, canvas.width, canvas.height)
		let soliddata = solid.data; 
		for(let i = 0; i < layerdata.length;i+=4){
			soliddata[i] = (layerdata[i]+layerdata[i+1]+layerdata[i+2]+layerdata[i+3])?255:0;
			soliddata[i+3] = (layerdata[i]+layerdata[i+1]+layerdata[i+2]+layerdata[i+3])?255:0;
		}
		context.putImageData(solid, 0, 0);

		canvas.check_collision = function(x, y){
			x = Math.round(x);
			y = Math.round(y);
			return soliddata[y * this.width * 4 + x * 4+3] === 255;
		}
		canvas.layer = layer.canvas;

		return canvas;
	}

    /**
     * Get layer info by layer name
     * @param {*} name 
     */
	getLayer(name){
		if(!this.data) return false;
		return this.data.layers.find(e=>{
			return e.name === name;
		})
	}

    /**
     * Fetch layers
     */
	getLayers(){
		if(!this.data) return false;
		return this.data.layers;
	}

    /**
     * Draw tile from tileset of the map
     * @param {Number} index 
     * @param {Number} x 
     * @param {Number} y 
     * @param {HTMLCanvasRenderingContext2D} context 
     */
	drawTile(index, x, y, context){
		let tilesetIndex = 0;
		let tileset = this.data.tilesets[0];
		let totalTiles = 0;
		for(let i = 0; i < this.data.tilesets.length;i++){
			tileset = this.data.tilesets[i];
			if(tileset.firstgid + tileset.tilecount > index)
				break;
		}
		tileset.rendering.context = context;
		tileset.rendering.draw(index - tileset.firstgid, x, y);
	}

    /**
     * Add new tileset to the map
     * @param {*} data - tileset data (tiled map editor format)
     * @param {*} callback - calls when tilemap is loaded
     */
	createTileset(data, callback){
		let isProduction = process.env.NODE_ENV === 'production';
		fetch(isProduction ? data.image.replace('..', '.') : data.image)
			.then(e=>{
				return e.blob();
			})
			.then(blob=>{
				let fr = new FileReader();
				fr.onload = function(e){
					let img = new Image();
					img.src = e.target.result;
					img.onload = e=>{
						callback({
							context: null,
							draw:function(index, x, y){
								if(!this.context) {
									console.error('not found context')
									return;
								}
								if(index === 0) return;
			
								let iy = Math.floor(index / data.columns); 
								let ix = index % data.columns
								this.context.drawImage(img, ix * data.tilewidth, iy * data.tileheight, data.tilewidth, data.tileheight, x * data.tilewidth, y * data.tileheight, data.tilewidth, data.tileheight)
							}
						})
					}
				}

				fr.onerror = function(){
					console.error('error with parsing')
				}
				fr.readAsDataURL(blob)
			})
			.catch(error=>{
				console.log('file not found ' + data.image);
			})
	}
}

export default TileMap