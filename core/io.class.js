import IOGamepad from './gamepad.io.class'

class IO {

    /**
     * Init input controller to Canvas element
     * @param {HTMLCanvasElement} canvas 
     */
	constructor(canvas){
		this.canvas = canvas;
		this.keyboard = [];
		this.__keyboard = []; //For Once
		this.keyboard.k_any = false;
		this.mouse = {
			x: 0,
			y: 0,
			pressed: false,
			right_pressed: false,
		};

		this.gamepad = new IOGamepad();
		this.gamepad.gamepad_create_emulation_map(0, {

		})


		//Keyboard
		window.addEventListener('keydown', e=>{
			this.keyboard.k_any = true;
			this.keyboard_handler(e, true);
		});
		window.addEventListener('keyup', e=>{
			this.keyboard.k_any = false;
			this.keyboard_handler(e, false);
		});

		//Mouse handlers
		window.addEventListener('mousedown', e=>{
			this.mouse.pressed = true;
		})
		window.addEventListener('mouseup', e=>{
			this.mouse.pressed = false;
		})

		this.canvas.addEventListener('mousemove', e=>{
			let scale = this.canvas.width / parseFloat(this.canvas.style.width) || 1;
			this.mouse.x = window.scrollX - this.canvas.getBoundingClientRect().left * scale + (e.pageX) * scale;
			this.mouse.y = window.scrollY - this.canvas.getBoundingClientRect().top * scale + (e.pageY) * scale;
		});

		// Const

		this.KEY_UP = 38
		this.KEY_LEFT = 37
		this.KEY_DOWN = 40
		this.KEY_RIGHT = 39
	}


	keyboard_handler(event, pressed){
		if(this.keyboard[event.keyCode] !== pressed) {
            this.keyboard[event.keyCode] = pressed;
        } else {
            this.__keyboard[event.keyCode] = false
		}
	}


	/**
	 * Call hander just once by key press
	 * @param  {string|number} str_code
	 * @param  {function} handler
	 * @return {void}
	 */
	keyboard_once(str_code, handler){
		if(str_code === 'any')
			return this.keyboard.k_any;

		if(typeof str_code === 'string')
			str_code = str_code.charCodeAt(0);

		if(this.keyboard[str_code]){
			if (!this.__keyboard[str_code]) {
				handler();
				this.__keyboard[str_code] = true
			}
			this.keyboard[str_code] = false
		}
	}


	/**
	 * Returns keyboard key state
	 * @param  {string|number} str_code - keyboard code
	 * @return {Boolean}
	 */
	keyboard_check(str_code){

		if(str_code === 'any')
			return this.keyboard.k_any;

		if(typeof str_code === 'string')
			str_code = str_code.charCodeAt(0);

		return this.keyboard[str_code] || false;
	}

	gamepad_connected(event){
		this.gamepad.push(event.gamepad);
	}


	gamepad_disconnected(event){
		this.gamepad.find((e,i)=>{
			if(e.index === event.gamepad.index){
				this.gamepad.splice(i, 1);
				return true;
			}
			return false;
		})
	}

	/**
	 * Returns gamepad button state
	 * @param  {number} - gamepad id
	 * @param  {number} - button id on gamepad
	 * @return {boolean}
	 */	
	gamepad_checkbutton(gpid, button_id){
		let gp = navigator.getGamepads()[gpid];
		if(!gp) return;

		return gp.buttons[button_id];
	}

	gamepad_create_emulation_map (gpid, map) {
		window.addEventListener('gamepadpressed', e => {
            let { gamepad_id, button_id } = e.detail
			if (gamepad_id !== gpid) return;
			if (map.buttons[button_id] !== undefined) {
				this.keyboard[map.buttons[button_id].charCodeAt(0)] = true;
			}
		})

        window.addEventListener('gamepadreleased', e => {
            let { gamepad_id, button_id } = e.detail
            if (gamepad_id !== gpid) return;
            if (map.buttons[button_id] !== undefined) {
                //this.keyboard[map.buttons[button_id].charCodeAt(0)] = false;
            }
        })

		window.addEventListener('gamepadaxemove', e => {
            let { gamepad_id, axe_id, value } = e.detail
            if (gamepad_id !== gpid) return;
            if (map.axes[axe_id] !== undefined) {
                this.keyboard[typeof map.axes[axe_id] === 'string' ? map.axes[axe_id].charCodeAt(0) : map.axes[axe_id]] = parseFloat(value.toFixed(2));
			}
		})
	}

	gamepad_vibrate (id, magnitude, duration) {
        this.gamepad.gamepad_vibrate(id, magnitude, duration)
	}
}

export default IO;
