class Font {
    constructor(url, size = 8) {
        this.image = new Image()
        this.image.src = url
        this.image.onload = () => {
            this.row = this.image.width / size
            if (this.onLoad) {
                this.onLoad(this)
            }
        }
        this.context = null
        this.scale = 1
        this.size = size
        this.row = 0
    }

    text(string, x, y) {
        let charStart = 33
        let ruStart = "А".charCodeAt(0) - 1
        let c = this.context
        let chars = string.split('')

        chars.forEach((char, i) => {
            let charID = char.charCodeAt(0)
            let ci = charID - charStart
            let cx, cy

            if (charID >= ruStart) {
                ci = ('~'.charCodeAt(0) - 33) + (charID - ruStart)
                console.log(ci, String.fromCharCode(ci))
            }

            cx = ci % this.row
            cy = ci / this.row >> 0
            c.save()
            c.translate(x, y)
            c.scale(1, 1)
            c.drawImage(this.image, cx * this.size, cy * this.size, this.size, this.size, Math.round(this.size * i + this.letterSpacing * i), 0, this.size, this.size)
            c.restore()
        })

        return {
            width: this.size * chars.length + this.letterSpacing * chars.length
        }
    }
}

export default Font;