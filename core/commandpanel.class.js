let frame = 0;
let carret = false



class CommandPanel {
    constructor (commandList) {
        let parent = this;
        this.commandList = Object.assign({
            clear: {
                method () {
                    parent.history = []
                }
            },
            command_list: {
                method () {
                    return Object.keys(parent.commandList)
                }
            }
        }, commandList)
        this.io = null;

        this.history = [
            'Foxal v1.0',
            'Command Panel'
        ]

        this.commandHistory = []
        this.commandHistoryIndex = 0

        this.autocomplete = []
        this.autocompleteCurrent = 0

        this.keyboardString = ''
        this.enabled = false

        window.addEventListener('keypress', this.__keyboardHandler.bind(this))
        window.addEventListener('keydown', this.__keyboardHandler.bind(this))
    }

    register (commandName, method, args, hint) {
        this.commandList[commandName] = { method, args, hint }
    }

    exec (string) {
        let commandName = string.split(' ')[0]
        let args = string.split(' ')
        args.splice(0, 1)

        if (this.commandList[commandName] !== undefined) {
            let result = this.commandList[commandName].method(args)
            if (result) {
                if (result instanceof Array) {
                    result.forEach(item => this.history.push('^2' + item))
                    return
                }
                this.history.push('^2' + result)
            }
        } else {
            this.history.push('^4Unknown command: ' + commandName)
        }
    }

    __keyboardHandler(event) {
        let ignore = [8, 13, 9];
        if (event.keyCode === 192) {
            this.enabled = !this.enabled
            this.keyboardString = ''
            event.preventDefault()
        }

        let makeAutocomplete = () => {
            let commandName = this.keyboardString.split(' ')[0].replace(/\^[0-9]/gi, '')

            let myPattern = new RegExp('([a-z0-9]*' + this.keyboardString.replace(/\^[0-9]/gi, '') + '[a-z0-9]*)', 'gi')
            if (commandName.length > 0 && this.keyboardString.length > 0) {
                this.autocomplete = Object.keys(this.commandList).filter((key, i) => {
                    if (key.match(myPattern)) {
                        return true
                    }
                })
            }
        }
        if (this.enabled) {
            if (event.type === 'keypress') {
                if (ignore.indexOf(event.charCode) > -1) {
                    return
                }
                this.keyboardString += event.key
                makeAutocomplete()
            }

            if (event.type === 'keydown') {
                switch (event.keyCode) {
                    case 8:
                        this.keyboardString = this.keyboardString.substr(0, this.keyboardString.length - 1)
                        makeAutocomplete()
                        break;
                    case 13:
                        this.history.push(this.keyboardString.replace(/\^[0-9]/gi, ''));
                        this.commandHistory.push(this.keyboardString.replace(/\^[0-9]/gi, ''));
                        this.exec(this.keyboardString.replace(/\^[0-9]/gi, '') );
                        this.keyboardString = '';
                        this.commandHistoryIndex = 0
                        break;
                    case 9:
                        event.preventDefault()
                        if (this.autocomplete.length > 0) {
                            this.keyboardString = this.autocomplete[0]
                        }
                        break;

                    case 40:
                        event.preventDefault()
                        if (this.autocomplete.length > 0) {
                            this.autocompleteCurrent++
                            if (this.autocompleteCurrent > this.autocomplete.length - 1)
                                this.autocompleteCurrent = this.autocomplete.length - 1
                            this.keyboardString = this.autocomplete[this.autocompleteCurrent]
                        } else {
                            this.commandHistoryIndex--
                            if (this.commandHistoryIndex < 0)
                                this.commandHistoryIndex = 0
                            this.keyboardString = this.commandHistory[this.commandHistoryIndex]
                        }
                        break;

                    case 38:
                        event.preventDefault()
                        if (this.autocomplete.length > 0) {
                            this.autocompleteCurrent--
                            if (this.autocompleteCurrent < 0)
                                this.autocompleteCurrent = 0
                            this.keyboardString = this.autocomplete[this.autocompleteCurrent]
                        } else {
                            this.commandHistoryIndex++
                            if (this.commandHistoryIndex > this.commandHistory.length - 1)
                                this.commandHistoryIndex = this.commandHistory.length - 1
                            this.keyboardString = this.commandHistory[this.commandHistoryIndex]
                        }
                        break
                }

                if (this.keyboardString === '') {
                    this.autocomplete = []
                    this.autocompleteCurrent = 0
                }
            }
        }


    }

    render (c) {
        if (this.enabled) {
            let renderColor = (str, x, y) => {
                let colors = {
                    '0': "#fff",
                    '1': "#00f",
                    '2': "#0f0",
                    '3': "#5af",
                    '4': "#f00",
                    '5': "#f0f",
                    '6': "#f80",
                    '7': "#ff0"
                }

                str = str.split('^')
                let xpos = 0
                str.forEach(word => {
                    if (isNaN(word[0])) {
                        c.fillStyle = colors[0];
                        c.fillText(word, x + xpos, y)
                        xpos += c.measureText(word).width
                    } else {
                        let w = word.substr(1, word.length)
                        c.fillStyle = colors[word[0]]
                        c.fillText(w, x + xpos, y)
                        xpos += c.measureText(w).width
                    }
                })
            }
            frame++
            c.textAlign = 'left'
            c.textBaseline = 'top'
            if (frame % 10 === 0) {
                carret = !carret
            }

            let {history} = this;
            c.fillStyle = 'rgba(0,0,0,0.5)';
            c.fillRect(0, 0, c.canvas.width, c.canvas.height / 2)

            c.fillStyle = "#fff";

            history.forEach((string, i) => {
                renderColor(string, 8, c.canvas.height / 2 - 32 - 16 * (history.length - i - 1))
            });

            c.font = '14px consolas';
            renderColor(this.keyboardString + (carret ? '_' : ''), 8, c.canvas.height / 2 - 16)

            let {autocomplete} = this
            if (autocomplete.length > 0) {
                c.fillStyle = "#000"
                c.fillRect(0, c.canvas.height / 2, c.canvas.width, autocomplete.length * 16)
                c.fillStyle = "#090"
                autocomplete.forEach((string, i) => {
                    if (this.autocomplete[this.autocompleteCurrent] === string) {
                        c.fillStyle = "#f80"
                    } else {
                        c.fillStyle = "#090"
                    }
                    c.fillText(string, 8 + 0.5, c.canvas.height / 2 + 14 + 16 * (i - 1) + 4)
                });
            }
        }
    }
}

export default CommandPanel