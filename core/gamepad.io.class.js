/**
 * Adds a new gamepad events
 * gamepadpressed, gamepadreleased, gamepadaxemove
 * @author h2xdeveloper@gmail.com
 */

class CuteGamepad {
    constructor () {
        this.gamepads = {}
        window.addEventListener('gamepadconnected', e => {
            this.gamepad_create(e.gamepad.index);
        })

        window.addEventListener('gamepaddisconnected', e => {
            this.gamepad_remove(e.gamepad.index);
        })

        let gamepads = navigator.getGamepads();
        for (let i = 0; i < gamepads.length; i++) {
            if (gamepads[i]) {
                this.gamepad_create(gamepads[i].index);
            }
        }

        this.update();
    }

    gamepad_create (id) {
        let gamepad = navigator.getGamepads()[id];
        this.gamepads[id] = {
            id,
            buttons: [],
            axes: []
        }


        for (let i = 0; i < gamepad.buttons.length; i++) {
            let button = gamepad.buttons[i];
            this.gamepads[id].buttons[i] = button.pressed;
        }
    }

    gamepad_remove(id) {
        this.gamepads.find((gamepad, i) => {
            if(gamepad.id === id) {
                this.gamepads.splice(i, 1);
                return true;
            }
        })
    }

    gamepad_vibrate (id, magnitude, duration) {
        let gamepad = navigator.getGamepads()[id];
        if (gamepad) {
            gamepad.vibrationActuator.playEffect(gamepad.vibrationActuator.type, {
                strongMagnitude: magnitude,
                weakMagnitude: magnitude,
                duration
            })
        }
    }

    gamepad_create_emulation_map (gpid, map) {
        window.addEventListener('gamepadpressed', e => {
            let { gamepad_id, button_id } = e.detail;
            if (gamepad_id !== gpid) return;
            let button = map[button_id];
            if (button !== undefined) {
                dispatchEvent(new Event('keydown', {
                    keyCode: typeof button === 'string' ? button.charCodeAt(0) : button
                }))
            }
        })

        window.addEventListener('gamepadreleased', e => {
            let { gamepad_id, button_id } = e.detail;
            if (gamepad_id !== gpid) return;
            let button = map[button_id];
            if (button !== undefined) {
                dispatchEvent(new Event('keyup', {
                    keyCode: typeof button === 'string' ? button.charCodeAt(0) : button
                }))
            }
        })
    }

    update () {
        let gamepads = navigator.getGamepads();
        for (let i = 0; i < gamepads.length; i++) {
            let gamepad = gamepads[i];
            if (gamepad) {

                /**
                 * Button controller
                 */
                for (let j = 0; j < gamepad.buttons.length; j++) {
                    let button = gamepad.buttons[j]
                    if (this.gamepads[gamepad.index].buttons[j] !== button.pressed) {
                        if (button.pressed) {
                            dispatchEvent(new CustomEvent('gamepadpressed', {
                                detail: {
                                    gamepad_id: i,
                                    button_id: j,
                                }
                            }))
                        } else {
                            dispatchEvent(new CustomEvent('gamepadreleased', {
                                detail: {
                                    gamepad_id: i,
                                    button_id: j
                                }
                            }))
                        }
                        this.gamepads[gamepad.index].buttons[j] = button.pressed
                    }
                }

                /**
                 * Axes controller
                 */

                for (let j = 0; j < gamepad.axes.length; j++) {
                    let axe = gamepad.axes[j]
                    if (this.gamepads[gamepad.index].axes[j] !== axe) {
                        dispatchEvent(new CustomEvent('gamepadaxemove', {
                            detail: {
                                gamepad_id: i,
                                axe_id: j,
                                value: axe
                            }
                        }))
                        this.gamepads[gamepad.index].axes[j] = axe
                    }
                }
            }
        }

        requestAnimationFrame(() => {
            this.update()
        })
    }
}

export default CuteGamepad;
