
class ResourceManager {

    /**
     * Init resource manager 
     * @param {function} onload - event calls when all files is loaded 
     */
	constructor(onload){
		this.files_loaded = 0;
		this.files_total = 0;
		this.files_preload = false;
		this.files_onload = onload;
	
		this.resources = {};
		this.resource_modules = {};
	}


    /**
     * Register loader module
     * @param {string} key - module name 
     * @param {*} handler - handler to load resource
     */
	register_module(key, handler){
		this.resource_modules[key] = handler;
	}


    /**
     * Start load resources
     * @param {*} callback - calls when all resources loaded
     */
	load_resources(callback){
		for(let key in this.resources){
			for(let resource in this.resources[key]){
				if(!this.resource_modules[key]) {
					console.warn('[FoxalJS 2nd] Can\'t find resource module "' + key + '"')
				}else{
					this.files_total++;
					this.resource_modules[key](this.resources[key][resource], result=>{
						this.resources[key][resource] = result;
						this.files_loaded++;
						if(this.files_loaded === this.files_total && !this.files_preload){
							this.files_onload();
							if(callback)
								callback()
							this.files_preload = true
						}
					})
				}
			}
		}
	}


    /**
     * Load local file to base64
     * @param {*} file - link to file
     * @param {*} callback - calls when file is loaded
     */
	load_file(file, type,  callback){
		this.files_total++;
		fetch(file)
		.then(e=>e[type]())
		.then(e=>{
			if (type === 'blob') {
                let fs = new FileReader();
                fs.onload = (event) => {
                    callback(event.target.result);
                    this.files_loaded++;
                };
                fs.readAsDataURL(e);
            } else {
                this.files_loaded++;
				callback(e)
			}
		})
	}
}

export default ResourceManager;