import Console from '../common/console'
import Particle from './particle.class'

class ParticleSystem {
    constructor () {
        this.particles = {
            list: {},
            length: 0,
            push (obj) {
                let temp = this.length;
                this.list[this.length] = obj;
                this.length++;
                return temp
            },

            kill (id) {
                if (this.list[id].onDestroy)
                    this.list[id].onDestroy()
                delete this.list[id]
            },

            killAll () {
                for (let index in this.list){
                    this.kill(index)
                }
            },

            update (context) {
                for (let index in this.list){
                    this.list[index].render(context)
                }
            }
        };
        this.types = {};
        this.shapes = {
            CIRCLE: 'circle',
            SPARK: 'spark'
        };

        this.areas = {
            CIRCLE: 'circle',
            SPARK: 'spark'
        }
    }

    type_destroyAll () {
        for (let type in this.types) {
            if (this.types[type].onDestroy)
                this.types[type].onDestroy()
            delete this.types[type]
        }
    }

    type_create (name, options) {
        if (this.types[name] !== undefined) {
            Console.warn('Particle type is defined already and was overwritten')
        }
        options.props = {}

        for (let key in options) {
            if (key !== 'props') {
                options.props[key] = options[key]
                delete options[key]
            }
        }

        options.name = name;
        options.type = 'particles';
        options.burst = (x, y, count) => {
            for (let i = 0; i < count; i++) {
                let p = new Particle(x, y, options.props);
                let pid = this.particles.push(p)
                p.on('destroy', () => {
                    this.particles.kill(pid)
                })
            }
        };

        options.stream = (x, y, count) => {
            return setInterval(() => {
                options.burst(x, y, count)
            }, 16)
        };

        options.stream_stop = (streamID) => {
            clearInterval(streamID)
        }

        this.types[name] = options;
        return this.types[name];
    }

    render (context) {
        this.particles.update(context)
    }
}

export default ParticleSystem