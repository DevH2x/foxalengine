let randomize = function (min, max) {
    if (min > max) return 0;
    return min + Math.random() * (max - min);
};

let randomKind = function (array) {
    return array[Math.floor(Math.random() * array.length)];
}

let assign = function (obj1, obj2) {
    for (let key in obj2) {
        if (typeof obj2[key] === 'object' && obj2[key] instanceof Array === false) {
            obj1[key] = assign({}, obj2[key])
        } else {
            obj1[key] = obj2[key]
        }
    }

    return Object.assign({}, obj1)
}

let toRad = 1 / 180 * Math.PI

class Particle {
    constructor (x, y, options) {
        let __default = {
            life: {
                min: 100,
                max: 200
            },
            color: '#fff',
            speed: {
                min: 1,
                max: 2
            },
            direction: {
                min: 0,
                max: 0
            },
            area: {
                type: 'square',
                params: {
                    width: 1,
                    height: 1
                }
            },
            shape: {
                type: 'circle',
                params: {
                }
            },
            alphaDependence: true,
            gravity: 0,
            vspeed: 0
        }

        this.x = x;
        this.y = y;


        this.events = { }

        this.properties = assign(__default, options)
        this.randomize()

        this.maxLife = this.properties.life
    }

    on (event, handler) {
        if (!this.events[event]) {
            this.events[event] = []
        }
        this.events[event].push(handler)
    }

    trigger (event) {
        if (!this.events[event]) return
        this.events[event].forEach(handler => {
            handler()
        })
    }

    randomize () {
        this.properties.life = randomize(this.properties.life.min || 0, this.properties.life.max || 0);
        this.properties.speed = randomize(this.properties.speed.min || 0, this.properties.speed.max || 0);
        this.properties.direction = randomize((this.properties.direction.min || 0) * toRad, (this.properties.direction.max || 0) * toRad);

        if (this.properties.color instanceof Array) {
            let { color } = this.properties
            this.properties.color = randomKind(color)
        }

        switch (this.properties.area.type) {
            case 'circle' :
                let angle = Math.random() * Math.PI * 2
                this.x += Math.cos(angle) * Math.random() * (this.properties.area.params.radius || 1)
                this.y += Math.sin(angle) * Math.random() * (this.properties.area.params.radius || 1)
                break

            case 'square' :
                let { width, height } = this.properties.area.params;
                this.x += Math.random() * (width || 1)
                this.y += Math.random() * (height || 1)
                break
        }

        switch (this.properties.shape.type) {
            case 'circle' :
                let min, max;
                if (this.properties.shape.params.radius) {
                    min = this.properties.shape.params.radius.min
                    max = this.properties.shape.params.radius.max
                }
                this.properties.shape.params.radius = randomize(min || 1, max || 1)
                break
        }
    }

    movement () {
        let { direction, speed, gravity, vspeed } = this.properties;
        this.properties.vspeed += gravity
        this.x += Math.cos(direction) * speed;
        this.y += Math.sin(direction) * speed + vspeed;
    }

    shape (shape, context) {
        let { x, y, properties } = this;
        let { direction, speed, vspeed } = properties;
        let { params } = properties.shape;
        switch (shape) {
            case 'circle':
                context.beginPath();
                context.arc(x, y, this.properties.shape.params.radius, 0, Math.PI * 2)
                context.fill()
                break

            case 'spark':
                let xd = Math.cos(direction) * speed;
                let yd = Math.sin(direction) * speed + vspeed;
                context.lineWidth = params.width || 1
                context.beginPath()
                context.moveTo(x + xd * (params.sensitivity || 1), y + yd * (params.sensitivity || 1) )
                context.lineTo(x, y)
                context.stroke()
                break
        }
    }

    lifecycle () {
        this.properties.life -= 1;
        if (this.properties.life < 0) {
            this.trigger('destroy')
        }
    }

    render (context) {
        this.lifecycle();
        this.movement();
        let { color, beforeDraw, afterDraw } = this.properties;
        context.fillStyle = context.strokeStyle = color;
        context.globalCompositeOperation = 'lighter'
        if (beforeDraw) beforeDraw(context)
        context.globalAlpha = this.properties.alphaDependence ? Math.max(0, this.properties.life / this.maxLife) : 1;
        this.shape(this.properties.shape.type, context)
        if (afterDraw) afterDraw(context)
        context.globalCompositeOperation = 'source-over'
        context.globalAlpha = 1;
    }
}

export default Particle;