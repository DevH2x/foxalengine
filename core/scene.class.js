class SceneController {

    /**
     * Init scene controller
     */
	constructor(renderer){
        if(!renderer){
            console.error('[FoxalJS: Scene] 1 argument required (renderer)')
        }else{
            this.canvas = renderer.canvas;
            this.context = renderer.context;
        }
		this.current = null;
		this.list = {};
		this.bgcolor = "#000";
	}

    /**
     * Register a new scene
     * @param {1} name - scene identificator
     * @param {object} detail - scene data
     */
	register(name, detail){
		detail.name = name;
		this.list[name] = detail;
	}

    /**
     * Preparing scene to working
     * @param {*} scene 
     */
	prepare(scene){
		scene.camera = scene.camera || {
			x: 0, y: 0, zoom: 0, rotate: 0
		}

		scene.cached = scene.cached || false;
	}

    /**
     * Loading scene to render
     * @param {*} name 
     */
	load(name){
        if(!this.canvas || !this.context) return false;
		let new_scene = this.list[name];
		if(!new_scene)
			return console.error('scene', name, 'not registered');

        this.current = new_scene;
		this.prepare(new_scene);
		new_scene.load();
	}

    /**
     * Rendering a scene frame
     */
	render(){
        if(!this.canvas || !this.context) return false;
		let context = this.context;
		let bgcolor = this.bgcolor;
		let canvas = this.canvas;
		context.fillStyle = bgcolor;
		context.fillRect(0,0, canvas.width, canvas.height);
		if(this.current){
			if(this.current.background) this.current.background(context);
			context.save();
            context.translate(canvas.width / 2, canvas.height / 2);
            context.rotate(this.current.camera.rotate)
            context.scale(this.current.camera.zoom || 1,this.current.camera.zoom || 1)
            context.translate(-canvas.width / 2, -canvas.height / 2);
            context.translate(-this.current.camera.x, -this.current.camera.y);
				if(this.current.before_update) this.current.before_update(context);
				if(this.current.update) this.current.update(context);
                if(this.current.after_update) this.current.after_update(context);
			context.restore();
			if(this.current.gui) this.current.gui(context);
		}
	}
}

export default SceneController;