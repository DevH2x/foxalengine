let scale = 0;
let style = '';
class Renderer {

    /**
     * Init render controller for rendering scenes and work different modules in realtime
     * @param {function} handler - render function for render loop
     */
	constructor(handler){
		this.render_function = handler;
		this.canvas = document.createElement('canvas');
		this.context = this.canvas.getContext('2d', { alpha: false, antialias: false });
		this.clear_color = "#000";
		this.renderer
		this.context.imageSmoothingEnabled = false
        this.context.runScript = function(script){
            let lines = script.split('\n');
            lines.forEach(e=>{
                e = e.replace(/\s\s+/g,'');
                if(e !== ''){
                    let a = e.split(' ');
                    if(typeof this[a[0]] === 'function')
                        this[a[0]](a[1],a[2],a[3],a[4], a[5], a[6], a[7], a[8]);
                    else
                        this[a[0]] = a[1];
                }
            })
        }
	}

	set scale(value){
		scale = value;
		this.canvas.style.width = this.canvas.width * value + 'px';
		this.canvas.style.height = this.canvas.height * value + 'px';
	}

	get scale(){
		return scale;
	}

	set style(value){
		style = value;
		this.canvas.style.imageRendering = value;
	}

	get style(){
		return style;
	}

    /**
     * Start render loop
     */
	render(){
		this.render_function(this.canvas, this.context);
		requestAnimationFrame(this.render.bind(this));
	}
}


export default Renderer;