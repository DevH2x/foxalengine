# Foxal Engine
The Game engine for development 2D games for Web, Mobile, Desktop and other...

The Engine is a module for development on webpack.
You need to import 'Core' folder to the entry file of your webpack project and begin to develop the game.

## Template to Start

__main.js__
```js
import Core from 'core';

let scene, renderer, render_loop;

scene = new Core.Scene();
scene.canvas = renderer.canvas;
scene.context = renderer.context;
scene.register('hello-world',{
    /*scene props*/
    load: function(){
        this.update = function(context){
            context.fillStyle = "#fff";
            context.fillText('Hello World', 16, 16)
        }
    }
})
renderer = new Core.Renderer(render_loop);

render_loop = function(canvas, context){
    scene.render();
};

render_loop.render();
```
