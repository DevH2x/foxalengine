class AuContext {
    /**
     * Simple creating AudioContext for sound effects
     * @param {*} source 
     * @param {*} effects 
     */
    constructor(sound, effects){
        if(!effects) return;
        let connecting = effects.split('-');
        this.actx = new AudioContext()
        this.effects = {};
        this.effects_queue = [];
        this.source = this.actx.createMediaElementSource(sound);
        connecting.forEach((e,i)=>{
            let params_regex = /\[.*?\]/gm
            let effectName = e.replace(params_regex, '');
            let params = e.match(params_regex)[0].replace('[', '').replace(']','').split(',');
            this.effects[effectName] = this.actx['create'+effectName]();
            this.effects_queue[i] = effectName;
            params.forEach(param=>{
                param = param.split('=');
                this.effects[effectName][param[0]]=param[1];
            })
        })

        this.source.connect(this.effects[this.effects_queue[0]]);
        this.effects_queue.forEach((e,i)=>{
            let effectName = e;
            let nextEffect = this.effects_queue[i+1];
            if(nextEffect)
                this.effects[effectName].connect(this.effects[next_effect])
            else
                this.effects[effectName].connect(this.actx.destination)
        })
    }
}

export default AuContext;