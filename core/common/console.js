export default {
    error (type, str) {
        console.error('[Foxal] >> ' + type + ': ' + str)
    },
    warn (type, str) {
        console.warn('[Foxal] >> ' + type + ': ' + str)
    }
}