
class Sprite {
    /**
     * Create sprite from texture
     * @param {HTMLImageElement|HTMLCanvasElement|SVGElement} texture - texture for the sprite
     */
	constructor(texture){
		this.frame_current = 0;
		this.frame_width = texture.width;
		this.frame_height = texture.height;
		this.frame_speed = 0.2;
		this.frame_count = 0;
		this.frame_cut = 0;
		this.offset = {
			x: 0, y: 0
		};

		this.scale = {
			x: 1, y: 1,
		};
		this.angle = 0;
		this.texture = texture;
		this.context = null;
	}

    /**
     * Set frame size
     * @param {Number} w - width of the frame
     * @param {Number} h - height of the frame
     */
	set_frame_size(w, h){
		this.frame_width = w;
		this.frame_height = h;
		let framex = Math.floor(this.texture.width / w);
		let framey = Math.ceil(this.texture.height / h);
		this.frame_count = framex * framey - 1;
	}

    /**
	 * Returns a clone of the Sprite
     * @returns {Sprite & Sprite}
     */
	clone () {
		return Object.assign(new Sprite(this.texture), this)
	}

    /**
     * Draw sprite to context
     * @param {*} x
     * @param {*} y
     */
	draw (x, y) {
		let c, frame_absolute;
		if(!this.context) return;
		c = this.context;
		frame_absolute = Math.floor(this.frame_current);

		this.frame_current += this.frame_speed;
		this.frame_current = this.frame_current > this.frame_count - this.frame_cut ? 0 : this.frame_current;

		let frames_width = Math.floor(this.texture.width / this.frame_width);
		let frame_x = frame_absolute % frames_width;
		let frame_y = frame_absolute / frames_width >> 0;

		c.save();
		c.translate(x, y);
		c.scale(this.scale.x, this.scale.y);
		c.rotate(this.angle / 180 * Math.PI);
		c.drawImage(
			this.texture,
			this.frame_width * frame_x, this.frame_height * frame_y,
			this.frame_width, this.frame_height,
			- this.offset.x, - this.offset.y,
			this.frame_width, this.frame_height
		);
		c.restore();
	}
}

export default Sprite;
