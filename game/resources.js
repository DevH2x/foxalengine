import SpriteModule from './resource_modules/sprite.module'
import MapModule from './resource_modules/map.module'
import SoundModule from './resource_modules/sound.module'
import FontModule from './resource_modules/font.module'
import ImageFontModule from './resource_modules/imageFont.module';

export default (manager) => {
    manager.resources = {
        sprites: {
            // Player
            player_run: { file: './gfx/player-run.png', w: 16, h: 16 },
            player_idle: { file: './gfx/player-idle.png', w: 16, h: 16 },
            player_fall: { file: './gfx/player-fall.png', w: 16, h: 16 },
            player_jump: { file: './gfx/player-jump.png', w: 16, h: 16 },
            player_impact: { file: './gfx/player-impact.png', w: 16, h: 16 },
            player_vestige: { file: './gfx/player-impact-ghost.png', w: 64, h: 64, o: { x: 32, y: 32 } },
            player_attack: { file: './gfx/player-attack.png', w: 16, h: 16 },
            player_ladder: { file: './gfx/player-ladder.png', w: 16, h: 16 },
            player_savepoint: { file: './gfx/player-savepoint.png', w: 16, h: 16, o: { x: 8, y: 8} },

            // Items
            item_pda: { file: './gfx/pda.png', w: 16, h: 16, o: { x: 8, y: 8 } },
            eye_target: { file: './gfx/eye-target.png', w: 32, h: 32, o: { x: 16, y: 16 } },

            // Mouse
            mouse_idle: { file: './gfx/mouse-idle.png', w: 24, height: 16, o: { x: 8, y: 12 } },
            mouse_run: { file: './gfx/mouse-run.png', w: 24, height: 16, o: { x: 8, y: 12 } },

            door_closed: { file: './gfx/door-locked.png' },
            door_unclosed: { file: './gfx/door-unlocked.png', w: 32, h: 48 },
            elevator: { file: './gfx/elevator1.png', w: 48, h: 24, o: {x: 0, y: 8} },

            vent: { file: './gfx/vent.png', o: {x: 16, y: 16} },
            explosion: { file: './gfx/explosion.png', w: 80, h: 80, o: { x: 40, y: 40 } },
            acid_drop: { file: './gfx/acid-drop.png', w: 8, h: 12, o: { x: 4, y: 4 } },

            // GUI
            bubble: { file: './gfx/bubble-window.png' }
        },
        maps: {
            map1: { file: './maps/map.json' },
            map2: { file: './maps/map2.json' },
            map3: { file: './maps/map3.json' },
            introduction: { file: './maps/introduction.json' },
            ch1r1: { file: './maps/test.json' },
            polygon: { file: './maps/test-polygon.json' },
            elevator_test: { file: './maps/elevator-test.json' },
            ground_test: { file: './maps/ground-test.json' }
        },
        sounds: {
            introduction: { file: './sounds/music/celestial_body_subterranean_ocean.mp3', volume: 0.5 },
            requiem: { file: './sounds/music/requiem_for_a_machine.ogg', volume: 0.5 },
            level0: { file: './sounds/music/level0.ogg', volume: 0.5, loop: true },

            // Player sounds
            player_step: { file: './sounds/step.ogg', volume: 0.7 },
            player_impact: { file: './sounds/ground.ogg', volume: 0.7 },
            player_jump: { file: './sounds/jump.ogg', volume: 0.7 },

            // Objects
            door_open: { file: './sounds/dooropen.ogg', volume: 0.7, rate: 1.4 },
            door_close: { file: './sounds/doorclose.ogg', volume: 0.7, rate: 1.4 },
            gate_open: { file: './sounds/gate_open.ogg', volume: 0.7, rate: 1.4 },
            gate_close: { file: './sounds/gate_close.ogg', volume: 0.7, rate: 1.4 },


            // Effects
            siren: { file: './sounds/siren.ogg', volume: 0.5, rate: 1 },
            vent: { file: './sounds/vent.ogg', volume: 1, rate: 1 },
            explosion: { file: './sounds/explosion.ogg', volume: 1, rate: 1 },
            punch_metal: { file: './sounds/punch_metal.ogg' },
            metal_slam: { file: './sounds/metal_slam.ogg', volume: 0.3 },
            player_restore: { file: './sounds/player_restore.ogg', volume: 0.5 },
            save_zone: { file: './sounds/save_zone.ogg', volume: 0.7 },
            click: { file: './sounds/click.ogg' },

            //No Touch Game,
            nt_win: { file: '/sounds/no-touch-game/win.wav'},
            nt_lose: { file: '/sounds/no-touch-game/lose.wav'},
            nt_start: { file: '/sounds/no-touch-game/start.wav'},
            nt_hit: { file: '/sounds/no-touch-game/hit.wav'},
            nt_direction: { file: '/sounds/no-touch-game/direction.wav'}
        },

        fonts: {
            main: {
                file: '/fonts/uni0553-webfont.woff',
                name: 'Main'
            }
        },
        imageFont: {
            main: {
                file: '/gfx/font-default.png',
                size: 9,
                letterSpacing: -3
            }
        }
    };

    manager.register_module('sprites', SpriteModule);
    manager.register_module('maps', MapModule);
    manager.register_module('sounds', SoundModule);
    manager.register_module('fonts', FontModule)
    manager.register_module('imageFont', ImageFontModule)
}

