import {scene, manager} from '../core'
import loadMap from './misc/map-loader'
import state from '../state';
import createObjectData from '../objects/common/object-data';

let bloomValue = 0
scene.register('engine', {
    // Scene props
    ref:            {},
    lights:         [],
    solid:          [],
    mapName:        'map3',
    spectateTo:     'plr',
    objects:        createObjectData(),
    dt: 0,

    // Methods
    createSolidArea (x, y, w, h) {
        let o = { x, y, w, h, objects: {} }
        this.solid.push(o)
        return o;
    },

    // On Scene was loaded...
    load() {
        // Scene working settings
        let frame =         0;
        let pulsePeriod =   32; // Pulsation lighting perion

        // Parsing map objects
        this.map =          manager.resources.maps[this.mapName];

        // manager.resources.sounds.introduction.play();
        let lighting =      loadMap(this, {ambient: 0x3a4e54, diffusion: 1 });

        // Get map parallax layers in background
        let parallaxesBefore = this.map.getLayers().filter(e=>{
            if (e.properties && e.properties.z !== undefined) {
                return (e.properties.z > 0)
            }
        })

        // Get map parallax layer in foreground
        let parallaxesAfter = this.map.getLayers().filter(e=>{
            if (e.properties && e.properties.z !== undefined) {
                return (e.properties.z < 0)
            }
        })

        // Rendering solid objects
        this.renderSolid = function (c) {
            c.strokeStyle = "#f00";
            c.lineWidth = 1;
            this.solid.forEach(area => {
                c.strokeRect(area.x, area.y, area.w, area.h)
            })
        }

        // Rendering lightmaps
        this.renderLighting = function (c) {
            if (frame % pulsePeriod === 0) {
                pulsePeriod = 16 + Math.round(Math.random() * 32);
                lighting.pulse.active = !lighting.pulse.active;
            }
            if (lighting.pulse.active) {
                lighting.pulse.drawLightmap(c, 0, 0, 3, this.camera);
            } else {
                lighting.static.drawLightmap(c, 0, 0, 3, this.camera);
            }
        }

        // Rendering objects
        this.renderObjects = function (c) {
            if (this.objects)
                this.objects.update(c);
        }

        this.renderGuiOfObjects = function (c) {
            if (this.objects) {
                this.objects.renderGui(c)
            }
        }

        // Rendering object in foreground
        this.renderObjectsAfter = function (c) {
            if (this.objects)
                this.objects.afterUpdate(c);
        }

        // Render map layers
        this.renderMap = function (c) {
            this.map.getLayers().forEach(layer => {
                if (layer.properties && layer.properties.z) return
                if (layer.canvas && layer.name[0] !== '$')
                    c.drawImage(layer.canvas, this.camera.x, this.camera.y, c.canvas.width, c.canvas.height, this.camera.x, this.camera.y, c.canvas.width, c.canvas.height)
            });
        }

        // TODO: Rewrite this!
        // Parallax Rendering
        this.renderBeforeParallax = function (c) {
            let __canvas = c.canvas;
            parallaxesBefore.forEach(layer => {
                let { z } = layer.properties;
                let { canvas } = layer
                let cx = this.camera.x + __canvas.width / 2
                let percent = (canvas.width / 2 - cx) / (canvas.width / 2)
                let x = Math.round(-64 * z * percent);
                let y = 0
                c.save()
                c.translate(x + canvas.width / 2, y + canvas.height / 2)
                c.globalAlpha = layer.opacity;
                c.drawImage(layer.canvas, - canvas.width / 2, - canvas.height / 2)` `
                c.globalAlpha = 1;
                c.restore()
            })
        }

        // TODO: Rewrite this!
        // Parallax Rendering in foreground
        this.renderAfterParallax = function (c) {
            let __canvas = c.canvas;
            parallaxesAfter.forEach(layer => {
                let { z } = layer.properties;
                let { canvas } = layer
                let cx = this.camera.x + __canvas.width / 2
                let percent = (canvas.width / 2 - cx) / (canvas.width / 2)
                let x = Math.round(-64 * z * (percent));
                let y = 0
                c.save()
                c.translate(x + canvas.width / 2, y + canvas.height / 2)
                c.globalAlpha = layer.opacity;
                c.drawImage(layer.canvas, - canvas.width / 2, - canvas.height / 2)
                c.globalAlpha = 1;
                c.restore()
            })
        }

        // Render particle system
        this.renderParticles = function (c) {
            if (this.ParticleSystem) {
                this.ParticleSystem.render(c)
            }
        }

        // Rendering dynamic lighting
        this.renderDynamicLight = function (c) {
            if (this.lights) {
                for (let i = 0; i < this.lights.length; i++){
                    let light = this.lights[i]
                    light.draw(c)
                    light.x = this.ref.plr.x;
                    light.y = this.ref.plr.y;
                }
            }
        }

        // TODO: Set flag in settings section
        // Rendering bloom
        this.bloomExperimental = function (c) {
            bloomValue -= (bloomValue - state.playerRespawn) / 10
            if (bloomValue < 0.1)  return
            c.globalCompositeOperation = 'screen'
            c.globalAlpha = bloomValue
            c.filter = 'blur(4px)'
            c.drawImage(c.canvas, 0, 0)
            c.filter = 'blur(0)'
            c.globalAlpha = 1
            c.globalCompositeOperation = 'source-over'
        }

        let _dt = 0
        this.update = function (c) {
            frame++;
            state.tick()
            // this.renderBeforeParallax(c)
            this.renderMap(c);
            this.renderParticles(c);
            this.renderObjects(c);
            this.renderLighting(c);
            this.renderDynamicLight(c);
            this.renderObjectsAfter(c);
            // this.renderAfterParallax(c)
            if (frame % 15 === 0) {
                this.dt = (performance.now() - _dt) / 15 / 1000
                _dt = performance.now()
            }
        }

        this.renderMiniGame = function (c) {
            if (state.game) {
                state.game.update(c)
            }
        }

        this.gui = function (c) {
            this.renderMiniGame(c)
            this.bloomExperimental(c);
            this.renderGuiOfObjects(c);
            state.guiObjects.forEach(object => object.update(c))
            c.fillStyle = "#fff"
            c.textAlign = "left"
            c.textBaseline = "top"
            c.font = "12px sans-serif"
            c.fillText('Performance: ' + Math.round(1 / this.dt / 61 * 100) + '%', 32, 32)
        }

        this.clear = function () {
            if (this.objects) {
                this.objects.destroyAll()
                for (let key in this.ref) {
                    delete this.ref[key]
                }
            }

            if (this.lights) {
                for (let i = 0; i < this.lights.length; i++) {
                    this.lights.splice(i--, 1)
                }
            }

            if (this.ParticleSystem) {
                this.ParticleSystem.type_destroyAll()
            }

            for (let i = 0; i < this.solid. length; i++) {
                this.solid.splice(i--, 1);
            }
        }

        this.mapLoad = function (mapName) {
            this.clear()
            this.mapName = mapName
            this.load()
        }

        this.after_update = function (c) {
            let obj = this.ref[this.spectateTo]
            let isPlayer = this.spectateTo === 'plr'
            if (obj) {
                this.camera.x -= (this.camera.x - (obj.x - c.canvas.width / 2 + Math.sign(Math.round(obj.speed.x)) * 50 * isPlayer)) / 8;
                this.camera.y -= (this.camera.y - (obj.y - c.canvas.height / 2 - 25 * isPlayer)) / 8;
                this.camera.x = Math.round(this.camera.x + Math.sin(frame * 2) * state.shake);
                this.camera.y = Math.round(this.camera.y + Math.cos(frame * 2) * state.shake);
            }
            if (isPlayer) {
                if (state.currentZone.width < c.canvas.width / 2) { return }
                let { plr }     = this.ref
                let fullWidth   = (state.currentZone.width)
                let plrDist     = (plr.x - state.currentZone.x)
                let percentX    = (plrDist / fullWidth)
                this.camera.x   -= (this.camera.x - (state.currentZone.x + (state.currentZone.width) * (percentX) - (c.canvas.width * percentX))) / 16

                // this.camera.x   = Math.max(state.currentZone.x, Math.min(this.camera.x, state.currentZone.x + state.currentZone.width - c.canvas.width))
                this.camera.x   = Math.round(this.camera.x)
            }
        }
    }
});
