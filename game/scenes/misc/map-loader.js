import { Lighting, ParticleSystem } from "../../../core";
import { objectsList } from "../../objects";
import { manager, scene as __scene } from "../../core";

let spawnObjects = (scene, lighting_options) => {
    let map = scene.map;
    if (map) {
        // Creating lighting
        let lighting = new Lighting(map.solid);
        let lightingPulse = new Lighting(map.solid);
        let globalAmbient = map.data.backgroundcolor;
        lighting.diffusion = lighting_options.diffusion;
        lighting.ambient = globalAmbient;
        lightingPulse.diffusion = lighting.diffusion;
        lightingPulse.ambient = lighting.ambient;
        lightingPulse.active = false;

        // Spawn Objects
        map.data.objects.forEach(objectsData => {
            let { objects } = objectsData
            objects.forEach(object => {
                object.props = {};
                if (object.properties) {
                    for (let i = 0; i < object.properties.length; i++) {
                        let prop = object.properties[i];
                        object.props[prop.name] = prop.value
                    }
                }
    
                if (objectsList[object.type] !== undefined) {
                    let ex = new objectsList[object.type];
                    ex.x = object.x;
                    ex.y = object.y - object.height * (object.gid !== undefined);
                    ex.width = object.width;
                    ex.height = object.height;
                    ex.properties = Object.assign(ex.properties || {}, object.props);
    
                    if (object.polyline) {
                        ex.path = object.polyline
                    }
    
                    if (object.name) {
                        scene.ref[object.name] = ex;
                        ex.name = object.name;
                    }
                    return;
                }
    
                // Check object is PointLight
                if (object.type === 'PointLight') {
                    let color = object.props.color.substr(3, 6); // Get light color
                    lightingPulse.addLight(object.x, object.y - map.data.tileheight + 8, 100 * object.props.scale, color);
                    if (object.props.pulse === false) {
                        lighting.addLight(object.x, object.y - map.data.tileheight + 8, 100 * object.props.scale, color);
                    }
                }
    
                // Analysing particle system
                if (object.type === 'Particles') {
                    if (scene.ParticleSystem === undefined) {
                        scene.ParticleSystem = new ParticleSystem()
                    }
    
                    let { name, props, x, y, width, height } = object;
                    let { type } = props;
                    manager.load_file('./particles/' + type + '.json', 'json', data => {
                        let o = new objectsList.ParticleType(scene.ParticleSystem.type_create(type, data), props);
                        o.x = x;
                        o.y = y;
                        if (name) {
                            scene.ref[name] = o
                        }
                    })
                }
            });
        })
        

        scene.objects.forEach(obj => {
            if (obj.onSpawn)
                obj.onSpawn()
        })

        scene.objects.forEachAfter(obj => {
            if (obj.onSpawn)
                obj.onSpawn()
        })

        // Render Lightmap
        lighting.render();
        lightingPulse.render();

        return { static: lighting, pulse: lightingPulse };
    }

    return false;
};

export default spawnObjects