import { scene, manager } from "../core";
import createObjectData from "../objects/common/object-data";
// import GUIParse from "../gui-renderer";
// import hud from '../gui/hud.xml'

scene.register('no-touch', {
    objects: createObjectData(),
    load () {
        let gui = new GUIParse(hud)

        this.update = context => {
            gui.render(context)
        }
    }
})