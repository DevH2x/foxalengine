import { io } from "./core";

class gui {
    constructor (xml) {
        this.xml = xml
        this.children = []
        this.attrs = this.parseAttrs()
        this.originalAttrs = Object.assign({}, this.attrs)
        this.hover = false
    }

    parseAttrs () {
        let result = {}
        if (!this.xml.attributes) return {}
        for (let i = 0; i < this.xml.attributes.length; i++) {
            let key = this.xml.attributes[i]
            if (!key) continue
            let isNumber = key.value === parseInt(key.value).toString()
            result[key.name] = isNumber ? parseInt(key.value) : key.value
        }
        return result
    }

    render (context) {
        let { width, height, x, y, fill, stroke, color } = this.attrs

        context.fillStyle = fill
        context.strokeStyle = stroke

        if (fill) {
            context.fillRect(x || 0, y || 0, width || 0, height || 0)
        }

        if (stroke) {
            context.strokeRect(x || 0, y || 0, width || 0, height || 0)
        }

        if(this.parent) {
            this.originalX = (this.parent.originalX || this.parent.attrs.x || 0) + (x || 0)
            this.originalY = (this.parent.originalY || this.parent.attrs.x || 0) + (y || 0)
        }

        
        this.hover = (io.mouse.x > this.originalX && io.mouse.x < this.originalX + width && io.mouse.y > this.originalY && io.mouse.y < this.originalY + height)

        for (let key in this.attrs) {
            let value = this.originalAttrs[key]
            let hoverValue = this.originalAttrs['hover-' + key] || value

            if (this.hover) {
                this.attrs[key] = hoverValue
            } else {
                this.attrs[key] = value
            }
        }

        context.save()
        context.translate(x || 0, y || 0)
        this.children.forEach(child => child.render(context))
        context.restore()
    }
}

class rectangle extends gui {
    constructor (xml) {
        super(xml)
    }
}

class button extends gui {
    constructor (xml) {
        super(xml)
        this.text = this.xml.innerHTML
        this.textHeight = 0
        this.hover = false
    }

    render (context) {
        super.render(context)
        let { text } = this
        let { width, height, x, y, fill, stroke, color, align, valign, onclick } = this.attrs

        context.font = "24px Main"
        
        if (!this.textHeight) {
            let e = document.createElement('div')
            e.style.fontSize = "24px"
            e.innerHTML = this.text
            document.body.appendChild(e)
            this.textHeight = e.offsetHeight
            document.body.removeChild(e)
        }

        if (io.mouse.pressed && this.hover) {
            eval(onclick)
            io.mouse.pressed = false
        }

        context.save()
        context.translate(x || 0, y || 0)
        if (text) {
            let _width, _height, ox, oy
            context.fillStyle = color
            context.textAlign = align || 'left'
            context.textBaseline = valign || 'top'
            text = text.replace(/\s+/g, '')
            _width = context.measureText(text).width
            _height = this.textHeight
            if (align) {
                switch (align) {
                    case 'center':
                        ox = width / 2
                    break;

                    case 'right':
                        ox = width - _width
                    break

                    default: 
                        ox = 0; 
                    break;
                }
            }

            if (valign) {
                switch (valign) {
                    case 'middle':
                        oy = height / 2
                    break

                    case 'bottom':
                        oy = height - _height
                    break

                    default: 
                        oy = 0
                    break
                }
            }

            context.fillText(text, ox, oy || 0)
        }

        context.restore()
    }
}

let ElementList = {
    gui,
    rectangle,
    button
}

let GUIParse = function(xml) {
    let parse = function (xml) {
        let el = new ElementList[xml.tagName || 'gui'](xml)
        for (let i = 0; i < xml.children.length; i++) {
            let child = xml.children[i]
            let childEl = parse(child)
            el.children.push(childEl)
            childEl.parent = el
        }
        return el
    }
    return parse(new DOMParser().parseFromString(xml, 'text/xml'))
}

export default GUIParse