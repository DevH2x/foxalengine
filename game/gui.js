class Element {
    constructor (arr) {
        this.width = 0
        this.height = 0
        this.x = 0
        this.y = 0

        this.style = {
            background: "#fff",
            color: "#333",
            borderColor: "#000",
            borderWidth: 2,
            marginX: 0,
            marginY: 0,
            paddingX: 0,
            paddingY: 0,
            font: '24px sans-serif',
            transform: {
                scaleX: 1,
                scaleY: 1,
                rotate: 0
            },
            textAlign: 'center'
        }

        this.events = {
            'click': [],
            'mouseenter': [],
            'mouseleave': []
        }

        this.content = ''

        this.children = arr || []
    }

    addEventListener (event, handler, options) {
        if (this.events[event]) {
            this.events[event].push(handler)
        }
    }

    removeEventListener (event, handler) {
        if (this.events[event]) {
            this.events[event].find((__handler, i) => {
                if (handler === __handler) {
                    this.events[event].splice(i, 1)
                    return true
                }
            })
        }
    }

    update (c) {
        let { x, y, children } =  this
        this.draw(c)
        children.forEach(element => {
            c.save()
            c.translate(x, y)
            element.update(c)
            c.restore()
        })
    }

    draw (c) {
        let { style, x, y, width, height, content, children } = this;
        let { background, color, borderColor, marginX, marginY, transform, borderWidth, font, textAlign } = style;

        c.font = font;

        if (!width) {
            this.width = c.measureText(content).width
        }

        if (!height) {
            this.height = font ? parseInt(font.split(' ')[0].replace('px', '').replace('em', '')) : 32
        }

        c.save();
        c.translate(x, y);
        c.scale(transform.scaleX, transform.scaleY);
        c.rotate(transform.rotate / 180 * Math.PI);

        c.fillStyle = background;
        c.fillRect(marginX, marginY, width, height);
        c.strokeStyle = borderColor;
        c.lineWidth = borderWidth;
        c.strokeRect(marginX, marginY, width, height);

        c.fillStyle = color;
        c.textAlign = textAlign;
        c.textBaseline = 'middle';
        let tx = 0, ty = height / 2;
        switch (textAlign) {
            case 'center':
                tx = width / 2
                break;

            case 'right':
                tx = width
                break;
        }

        c.fillText(content, tx, ty);

        c.restore()
    }
}

class Window extends Element {
    constructor (arr) {
        super(arr)
    }
}

export { Element }
