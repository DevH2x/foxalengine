import Engine, { ParticleSystem } from '../core'
import renderer from './renderer'
import manager from './manager'
import io from './io'

// Creating Scene controller
let scene = new Engine.Scene( renderer );
renderer.render();

export { renderer, scene, io, manager, ParticleSystem }
export default {
    renderer, scene, io, manager,
    init (callback) {
        document.body.appendChild(renderer.canvas);
        manager.load_resources(callback);
    }
}
