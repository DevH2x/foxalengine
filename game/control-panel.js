
import CommandPanel from "../core/commandpanel.class";
import manager from './manager'
import { scene } from './core'
import state from "./state";
let commandPanel = new CommandPanel({

    'version': {
        method () {
            return `^4███████╗ ██████╗ ██╗  ██╗ █████╗ ██╗     
^6██╔════╝██╔═══██╗╚██╗██╔╝██╔══██╗██║     
^7█████╗  ██║   ██║ ╚███╔╝ ███████║██║     
^2██╔══╝  ██║   ██║ ██╔██╗ ██╔══██║██║     
^3██║     ╚██████╔╝██╔╝ ██╗██║  ██║███████╗
^5╚═╝      ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝`.split('\n')
        }
    },

    'map': {
        method (args) {
            if (!manager.resources.maps[args[0]]) return 'Map ' + args[0] + ' is not found';
            if (scene.current.name === 'engine') {
                scene.current.mapLoad(args[0])
            } else {
                scene.load('engine')
                this.method(args)
            }
        }
    },

    maplist: {
        method (args) {
            return Object.keys(manager.resources.maps)
        }
    },

    'restart': {
        method (args) {
            if (scene.current.name === 'engine') {
                commandPanel.exec('map ' + scene.current.mapName)
            } else {
                return 'You have not loaded map'
            }
        }
    },

    entities: {
        method () {
            return ['Entities: '].concat(Object.keys(scene.current.ref))
        }
    },

    'entfire': {
        method (args) {
            let entity = args[0]
            let command = 'scene.current.ref.' + entity + '.' + args[1]
            eval(command)
            return ''
        }
    },

    renderer: {
        method (args) {
            renderer[args[0]] = parseFloat(args[1])
        }
    },

    debug: {
        method (args) {
            let boolean = {
                false: false,
                true: true
            }
            state.debug = boolean[args[0]] || false

            return '^3set debug mode to ' + state.debug.toString()
        }
    },

    log_objects_data: {
        method (args) {
            console.log(scene.current.objects)
            return '^2Check the browser console'
        }
    },
    log_scene_data: {
        method (args) {
            console.log(scene.current)
            return '^2Check the browser console'
        }
    },
    log_state: {
        method (args) {
            console.log(state);
            return '^2Check the browser console'
        }
    }
})

export default commandPanel