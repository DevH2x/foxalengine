import Engine from '../core'
import renderer from './renderer'
// Creating I/O controller
let io = new Engine.IO( renderer.canvas );
io.gamepad_create_emulation_map(0, {
    buttons: {
        14: 'A',
        15: 'D',
        0: ' ',
        2: 'X',
        1: 'C'
    },

    axes: {
        0: io.KEY_RIGHT,
    	1: io.KEY_DOWN
    }
})

export default io;
