import Actor from "./entities/actor";
import { manager } from "../core";
import state from "../state";

class AcidDrop extends Actor {
    constructor () {
        super()
        this.sprite = manager.resources.sprites.acid_drop.clone()
        this.pixelStrict = true
        this.speed.y = -6
    }

    update (c) {
        super.update(c)
        this.speed.y += state.gravity
        this.sprite.angle = (Math.atan2(this.speed.y, this.speed.x) + Math.PI / 2) * 180 / Math.PI

        if (this.check_collision(this.x, this.y + this.speed.y)) {
            this.scene.objects.destroy(this.id)
        }

        if (this.playerDistance < 16) {
            this.scene.ref.plr.damage()
        }
    }
}

export default AcidDrop