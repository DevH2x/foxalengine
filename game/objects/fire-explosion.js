import Actor from "./entities/actor";
import animation, { ease } from "../animation";

class FireExplosion extends Actor {

    constructor (x, y) {
        super()
        this.rayLength = 1
        this.coords = new Array(100)
        this.angle = (Math.PI * 2) / this.coords.length
        this.x = x
        this.y = y

        animation(percent => {
            this.rayLength = 110 * ease.easeOutCubic(percent)
        }, 500)
    }

    ray(x, y, direction, length) {
        for (let i = 0; i < length; i++) {
            if (this.check_collision(x + Math.cos(direction) * i, y + Math.sin(direction) * i)) {
                return { 
                    x: x + Math.cos(direction) * i,
                    y: y + Math.sin(direction) * i
                }
            }
        }
        return { 
            x: x + Math.cos(direction) * length,
            y: y + Math.sin(direction) * length
        }
    }

    update (c) {
        super.update(c)
        c.fillStyle = c.createLi(this.x, this.y, 0, this.x, this.y, this.rayLength)
        c.fillStyle.addColorStop(1, "rgba(255, 0, 0, 0)")
        c.fillStyle.addColorStop(0, "rgba(255, 255, 255, 1)")
        c.strokeStyle = c.fillStyle
        c.beginPath()
        for (let i = 0; i < this.coords.length; i++) {
            if (this.rayLength < 200) {  
                this.coords[i] = this.ray(this.x, this.y, this.angle * i, this.rayLength)
            }
            let { x, y } = this.coords[i]
            c[i === 0 ? 'moveTo' : 'lineTo'](x, y)
        }
        c.globalCompositeOperation = "color-dodge"
        c.fill()
        c.stroke()
        c.globalCompositeOperation = "source-over"
    }
}

export default FireExplosion