import state from "../../state";
import Actor from "./actor";

class Area extends Actor{
    constructor () {
        super()
        this.playerInArea = false
        this.__debugColor = "#f00"
    }

    update (c) {
        if (state.debug) {
            c.strokeStyle = this.__debugColor
            c.strokeRect(this.x, this.y, this.width, this.height)
        }
        if (this.scene.ref) {
            let { plr } = this.scene.ref;
            if (plr) {
                this.playerInArea = (
                    plr.x > this.x && plr.x < this.x + this.width && 
                    plr.y > this.y && plr.y < this.y + this.height
                )
            }
        }
    }
}

export default Area