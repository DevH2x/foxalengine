import Actor from "./actor";
import io from "../../io";
class Item extends Actor {
    constructor () {
        super()
        this.name = "none"
        this.frame = 0
    }

    update (c) {
        let sin = Math.round(Math.sin(this.frame / 20) * 4)
        super.update(c)
        this.frame++
        this.SPRITES.eye_target.draw(this.x, this.y + sin)
        this._sprite.draw(this.x, this.y + sin)
        if (this.playerDistance < 32) {
            io.keyboard_once('X', () => {
                if (this.onUse) {
                    this.onUse()
                    this.SOUNDS.click.play()
                }
            })
        }
    }
}

export default Item