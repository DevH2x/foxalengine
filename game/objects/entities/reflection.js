import Actor from "./actor";
import { scene } from "../../core";

class Reflection extends Actor {
    constructor () {
        super(true)
        this.x = 0;
        this.y = 0;
        this.width = 0;
        this.height = 0;
    }

    checkInScene (c) {
        let { canvas } = c
        let {x, y, width, height, scene } = this
        let { camera } = scene
        if (x + width > camera.x && x < camera.x + canvas.width && y + height > camera.y && y < camera.y + canvas.height) {
            return true
        }
        return false
    }

    // beforeUpdate (c) {
    //     if (!this.checkInScene(c)) return
    //     let { x, y, width, height } = this
    //     let cropX = (x - this.scene.camera.x)
    //     let cropY = (y - this.scene.camera.y - height)
    //     let cropWidth = width
    //     let cropHeight = height
    //     c.globalAlpha = this.properties.opacity || 1
    //     c.globalCompositeOperation = 'lighter'
    //     c.save()
    //     c.translate(x, y)
    //     c.scale(1, -1)
    //     c.drawImage(scene.canvas, cropX, cropY, cropWidth, cropHeight, 0, -height, width, height )
    //     // c.filter = 'blur(4px)'
    //     // c.drawImage(scene.canvas, cropX, cropY, cropWidth, cropHeight, 0, -height, width, height )
    //     // c.filter = 'none'
    //     c.globalCompositeOperation = 'source-over'
    //     c.restore()
    //     c.globalAlpha = 1

    //     let grad = c.createLinearGradient(x + width / 2, y, x + width / 2, y + height)
    //     grad.addColorStop(0, "white")
    //     grad.addColorStop(0.7, "black")

    //     c.globalCompositeOperation = 'overlay'
    //     c.fillStyle = grad;
    //     c.fillRect(x, y, width, height)
    //     c.globalCompositeOperation = 'source-over'
    // }
}


export default Reflection