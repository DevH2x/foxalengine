import { scene, manager } from '../../core.js';

class Actor {
	constructor(afterAll) {
		this.id 			= Date.now().toString(35)
		this.x 				= 0;
		this.y 				= 0;
		this.pixelStrict 	= true;
		this.speed = {
			x: 0,
			y: 0
		};
		this.sprite 		= null;
		this.direction 		= 0;
		this.scene 			= scene.current;
		this.playerDistance = 999999;
		this.afterAll 		= afterAll
		this.friction 		= { x: 1, y: 1 };
		this.inCamera 		= false

		this.SPRITES 		= manager.resources.sprites
		this.SOUNDS 		= manager.resources.sounds
		this.FONT 			= manager.resources.imageFont

		this.scene.objects.push(this);
	}

	check_collision (x, y) {
		if (this.scene) {
			let { map, solid } = this.scene;
			if (map) {
				// Checking collision in tiled map
				if (map.solid.check_collision(x, y)) {
					return true;
				}

				// Checking collision in solid objects
				for (let i = 0, length = solid.length; i < length; i++) {
					let area 		= solid[i]
					let isInArea 	= (
						x >= area.x && x <= area.x + area.w && 
						y >= area.y && y <= area.y + area.h
					)

					if (isInArea) {
						if (!!area.objects[this.id] === false) {
							area.objects[this.id] = this;
						}
                        return true;
					} else {
						if (!!area.objects[this.id] === true) {
                            delete area.objects[this.id]
                        }
					}
				}
			}
		}

		return false;
	}

	update (c) {
		this.inCamera = (this.x > this.scene.camera.x && this.x < this.scene.camera.x + c.canvas.width && this.y > this.scene.camera.y && this.y < this.scene.camera.y + c.canvas.height)

		if (!this.scene.ref) return
		let { plr } = this.scene.ref;
		if (plr) {
            let x1 = this.x
            let y1 = this.y

            let x2 = plr.x
			let y2 = plr.y
            this.playerDistance = Math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)
		}
	}

	afterUpdate () {
		this.x += this.speed.x;
		this.y += this.speed.y;

		if(this.pixelStrict){
			this.x = Math.round(this.x);
			this.y = Math.round(this.y);
		}
		
		this.speed.x *= this.friction.x;
		this.speed.y *= this.friction.y;

		if (this.sprite && this.inCamera) {
			this.sprite.draw(this.x, this.y);
		}
	}
}


export default Actor;

