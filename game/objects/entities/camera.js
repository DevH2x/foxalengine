import Actor from "./actor";
import {scene} from "../../core";

class Camera extends Actor {
    onSpawn () {
        let { showOnSpawn } = this.properties
        this.track = this.scene.ref[this.properties.track]
        this.currentPoint = 0;
        if (showOnSpawn) {
            this.enable()

            this.x = this.track.path[0].x
            this.y = this.track.path[0].y

            this.scene.camera.x = this.x - scene.canvas.width / 2;
            this.scene.camera.y = this.y - scene.canvas.height / 2;
        }
        this.temp = null;
        this.pixelStrict = false
        this.isEnd = false;
        console.log(this.scene.ref)
    }

    enable () {
        this.temp = this.scene.spectateTo
        this.scene.spectateTo = this.name
    }

    disable () {
        this.scene.spectateTo = 'plr'
    }

    update (c) {
        if (this.scene.spectateTo === this.name) {
            if (!this.isEnd) {
                let { x, y } = this
                let targetPoint = this.track.path[this.currentPoint]
                let dir = Math.atan2((targetPoint.y) - y, (targetPoint.x) - x)
                let dist = Math.sqrt((targetPoint.x - x) ** 2 + (targetPoint.y - y) ** 2)

                this.x += Math.cos(dir) * this.properties.speed
                this.y += Math.sin(dir) * this.properties.speed

                if (dist < this.properties.speed) {
                    if (this.track.path.length - 1 !== this.currentPoint) {
                        this.currentPoint++
                    } else {
                        this.isEnd = true
                        this.onTrackEnd()
                    }
                }
            }
        }
    }

    onTrackEnd () {
        let { onTrackEnd, refTarget } = this.properties
        if (onTrackEnd) {
            eval('this.scene.ref.' + refTarget + '.' + onTrackEnd)
        }
    }

    beforeDestroy () {
        this.disable()
    }
}

export default Camera