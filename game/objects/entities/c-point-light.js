import { scene } from "../../core";

class CPointLight {
    constructor () {
        this.canvas = document.createElement('canvas');
        this.baseWidth = 100;
        this.properties = {
            color: '#ffffffff',
            scale: 2,
            enabled: true
        }
        this.posX = 0
        this.posY = 0

        this.image = null

        if (!scene.current.lights) {
            scene.current.lights = []
        }

        this.render()
        scene.current.lights.push(this)
        this.scene = scene.current
        this.inCamera = false
    }

    get x () {
        return this.posX
    }

    set x (value) {
        if (this.posX !== value) {
            this.posX = value
            this.render()
        }
    }

    get y () {
        return this.posY
    }

    set y (value) {
        if (this.posY !== value) {
            this.posY = value
            this.render()
        }
    }

    render () {
        let { baseWidth, canvas } = this;
        let { scale, color } = this.properties;
        let { map } = scene.current;
        let radius = baseWidth * scale;
        let context = canvas.getContext('2d');

        let rays = 360
        let angle = (Math.PI * 2) / 270


        canvas.width = radius;
        canvas.height = radius;

        // map.solid.check_collision(x + (this.sprite.frame_width / 4) * sign + i * sign, y)
        if (map) {
            let ray = (dir) => {
                let x = Math.cos(dir)
                let y = Math.sin(dir)
                let i = 0;
                for (i = 0; i < radius / 2; i++) {
                    if (map.solid.check_collision(this.posX + x * i, this.posY + y * i)) {
                        break;
                    }
                }
                return {
                    x: x * i,
                    y: y * i
                }
            }

            let r = ray(0)
            context.fillStyle = '#' + color.substr(3, 6);
            context.beginPath()
            context.moveTo(radius / 2 + r.x, radius / 2 + r.y)
            for (let i = 1; i < rays; i++) {
                r = ray(angle * i)
                context.lineTo(radius / 2 + r.x, radius / 2 + r.y)
            }
            context.fillStyle = context.createRadialGradient(radius / 2, radius / 2, 0, radius / 2, radius / 2, radius / 2);
            context.fillStyle.addColorStop(0.9, '#000');
            context.fillStyle.addColorStop(0.0, '#' + color.substr(3, 6));
            context.fill()
        }
    }

    draw (c) {
        this.inCamera = (this.x > this.scene.camera.x && this.x < this.scene.camera.x + c.canvas.width && 
                            this.y > this.scene.camera.y && this.y < this.scene.camera.y + c.canvas.height)
        if (this.properties.enabled && this.inCamera) {
            let {canvas} = this;
            let {width, height} = canvas;
            c.globalCompositeOperation = 'color-dodge'
            c.drawImage(canvas, this.posX - width / 2, this.posY - height / 2)
            c.globalCompositeOperation = 'source-over'
        }
    }

    destroy () {
        scene.current.lights.find((light, i) => {
            if (light === this) {
                scene.current.lights.splice(i, 1)
                return true
            }
        })
    }

}

export default CPointLight