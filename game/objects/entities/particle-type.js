import {scene} from "../../core";

export default class ParticleType {
    constructor (pt, options) {
        this.pt = pt;
        this.x = 0;
        this.y = 0;
        this.props = options;
        this.interval = null;

        if (options.streamOnSpawn) {
            this.stream()
        }

        pt.onDestroy = this.beforeDestroy.bind(this)
    }

    stream () {
        let { props } = this;
        let { streamInterval } = props;
        this.interval = setInterval(() => {
            this.burst()
        }, streamInterval || 1)
    }

    __stream (int) {
        let { props } = this;
        let { streamInterval } = props;
        this.interval = setInterval(() => {
            this.burst()
        }, 1)
    }

    burst () {
        let { x, y, pt, props } = this;
        let { particlesPerFrame } = props;
        let { camera } = scene.current;
        let { canvas } = scene;
        if (x > camera.x && x < camera.x + canvas.width && y > camera.y && y < camera.y + canvas.height) {
            pt.burst(x, y, particlesPerFrame || 1)
        }
    }

    beforeDestroy () {
        console.log(this.interval)
        clearInterval(this.interval)
        console.log(this.interval)
        this.pt = null
    }
}