import Area from "./area";
import DisplayText from "./display-text";
import state from "../../state";
import { manager } from "../../core";

class SaveZone extends Area {
    constructor () {
        super()
        this.__debugColor = "#0f0"
    }

    afterUpdate (c) {
        if (this.playerInArea && state.saveZoneObject !== this) {
            this.scene.ref.plr.save()
            state.saveZone = true
            state.saveZoneObject = this
            new DisplayText(this.properties.zone || 'Unknown zone')
            manager.resources.sounds.save_zone.play()
        }
    }
}

export default SaveZone