import { io, manager } from "../../core";
import animation, { ease } from "../../animation";
class TouchLocker {
    constructor (speed = 3, plr = 16, grid = 32, map) {
        let y = 0

        this.canvas = document.createElement('canvas')
        this.c = this.canvas.getContext('2d')
        this.map = map || [
            [ 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
            [ 1, 1, 0, 0, 0, 0, 0, 1, 1 ],
            [ 1, 1, 0, 1, 1, 1, 0, 1, 1 ],
            [ 1, 1, 0, 1, 0, 0, 0, 1, 1 ],
            [ 1, 1, 0, 1, 0, 1, 1, 1, 1 ],
            [ 1, 1, 0, 1, 0, 0, 0, 0, 0 ],
            [ 0, 0, 0, 1, 1, 1, 1, 1, 1 ],
            [ 1, 1, 1, 1, 1, 1, 1, 1, 1 ]
        ]

        this.canvas.width = this.map[0].length * grid
        this.canvas.height = this.map.length * grid
        
        this.map.find((row, i) => {
            if (!row[0]) {
                y = i;
                return true;
            }
        })

        this.pl = {
            x: 0,
            y: y * grid + grid / 2,
            start () {
                this.x = 0,
                this.y = y * grid + grid / 2
                this.dir = 0
            },
            dir: 0,
            size: plr
        }

        this.grid = grid
        this.speed = speed;
        this.dir = 0
        this.coldown = 120
        this.won = false
        this.scale = 0

        animation(percent => {
            this.scale = percent
        }, 500)

        manager.resources.sounds.nt_start.play()
    }

    isSolid (x, y) {
        x = Math.floor(x / this.grid)
        y = Math.floor(y / this.grid)

        return this.map[y] ? this.map[y][x] : 0
    }

    update (context) {
        let { c, canvas, map, dir, speed } = this
        let { cos, sin, PI } = Math
        let { sounds } = manager.resources

        context.filter = 'blur(8px)'
        context.drawImage(context.canvas, 0, 0)
        context.filter = "blur(0)"
        context.save()
        context.translate(context.canvas.width / 2, context.canvas.height / 2)
        if (!this.won) {
            let s = ease.easeOutElastic(this.scale)
            let p = ease.easeOutQuad(this.scale)
            context.scale(s, s)
            context.rotate(PI / 4 - p * PI / 4)
        }
        context.drawImage(canvas, -canvas.width / 2, -canvas.height / 2)

        context.globalCompositeOperation = "screen"
        context.filter = "blur(4px)"
        context.drawImage(canvas, -canvas.width / 2, -canvas.height / 2)
        context.filter = "blur(0)"
        context.globalCompositeOperation = "source-over"

        context.restore()

        c.fillStyle = "#111"
        c.fillRect(0, 0, canvas.width, canvas.height)
        c.lineWidth = 2;
        c.strokeStyle = "#999"
        c.strokeRect(0, 0, canvas.width, canvas.height)
        

        c.fillStyle = "#333"

        let i = 0
        map.forEach((row, y) => {
            row.forEach((cell, x) => {
                if (!cell) {
                    i++
                    c.fillStyle = `hsl(${15 * i + this.coldown}deg, 100%, 25%)`
                    c.fillRect(x * 32, y * 32, 32, 32)
                }
            })
        })

        c.fillStyle = "#f4428c"
        c.fillRect(this.pl.x - this.pl.size / 2, this.pl.y - this.pl.size / 2, 16, 16)

        if (--this.coldown > 0) { 
            c.fillStyle = "#fff"
            c.font = "32px Main"
            c.textAlign = "center"
            c.textBaseline = "middle"
            c.globalAlpha = Math.max(0, Math.round(sin(this.coldown / 4)))
            c.fillText("GET READY", canvas.width / 2, canvas.height / 2)
            c.globalAlpha = 1
        } else {
            this.pl.x += cos(this.pl.dir) * speed
            this.pl.y += sin(this.pl.dir) * speed

            // Controls

            io.keyboard_once(io.KEY_UP, e => {
                this.pl.dir = -PI / 2
                sounds.nt_direction.play()
            })
            io.keyboard_once(io.KEY_LEFT, e => {
                this.pl.dir = PI
                sounds.nt_direction.play()
            })
            io.keyboard_once(io.KEY_DOWN, e => {
                this.pl.dir = PI - PI / 2
                sounds.nt_direction.play()
            })
            io.keyboard_once(io.KEY_RIGHT, e => {
                this.pl.dir = 0
                sounds.nt_direction.play()
            })
        }

        // Lose
        if (this.pl.x < 0 || this.isSolid(this.pl.x, this.pl.y)) {
            this.pl.start()
            sounds.nt_lose.play()
            sounds.nt_hit.play()
            animation(percent => {
                this.scale = 1 - percent
                this.speed = 0
                if (this.onLose) {
                    this.onLose()
                }
            }, 1000)
        }

        // Win
        if (this.pl.x > canvas.width && !this.won) {
            this.won = true
            sounds.nt_win.play()
            animation(percent => {
                this.scale = ease.easeOutElastic(percent)
            }, 500)
            .then(() => {
                animation(percent => {
                    this.scale = ease.easeOutElastic(1 - percent)
                    if (this.onUnlock) {
                        this.onUnlock()
                    }
                }, 500)
            })
        }

        if (this.won) {
            let { scale } = this
            c.fillStyle = "#fff"
            c.font = "32px Main"
            c.textAlign = "center"
            c.textBaseline = "middle"
            c.save()
            c.translate(canvas.width / 2, canvas.height / 2)
            c.scale(scale, scale)
            c.fillText("UNLOCKED SUCCESS", 0, 0)
            c.restore()
        }
    }
}

export default TouchLocker