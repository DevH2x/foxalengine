import Area from "./area";
import { manager } from "../../core";
import state from "../../state";

class MusicZone extends Area {
    constructor () {
        super()
        this.__debugColor = "#0ff"
        this.music = null
        this.isPlaying = false
    }

    onSpawn () {
        this.music = manager.resources.sounds[this.properties.music]
    }

    update (c) {
        super.update(c)
        if (this.playerInArea) {
            if (state.currentMusic !== this.music) {
                if (state.currentMusic) {
                    let shapshot = state.currentMusic
                    state.currentMusic.fade(0.5, 0, 500)
                    setTimeout(() => {
                        shapshot.pause()
                    }, 500)
                }                    
                this.music.play()
                this.music.fade(0, 0.5, 500)
                state.currentMusic = this.music
            }
        }
    }
}

export default MusicZone