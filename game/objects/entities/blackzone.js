import Actor from "./actor";
import state from "../../state";

class BlackZone extends Actor {
    constructor () {
        super()
        this.inside = false;
        this.alpha = 1;
        this.afterAll = true
    }

    onSpawn () {
        this.scene.objects.swap(this.id, 999 + this.id)
    }

    isVisible (c) {
        let { camera } = this.scene
        return (this.x + this.width > camera.x && this.x < camera.x + c.canvas.width &&
            this.y + this.height > camera.y && this.y < camera.y + c.canvas.height)
    }

    drawBlackout (c) {
        if (!this.isVisible(c)) return
        c.globalAlpha = this.alpha;
        c.fillStyle = "#000";
        c.shadowBlur = 4
        c.shadowColor = "#000"
        c.fillRect(this.x, this.y, this.width, this.height)
        c.shadowBlur = 0
        c.globalAlpha = 1;
    }

    afterUpdate (c) {
        let { plr } = this.scene.ref
        let { x, y, width, height } = this;

        if (plr) {
            this.inside = (plr.x > x && plr.x < x + width && plr.y > y && plr.y < y + height)
            if (this.inside) {
                state.currentZone = this
            }
        }

        this.alpha -= (this.alpha - (this.inside ? 0 : 1)) / 8;
        this.drawBlackout(c)
    }
}

export default BlackZone