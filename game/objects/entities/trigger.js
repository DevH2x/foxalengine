import Actor from "./actor";

export default class Trigger extends Actor {
    constructor () {
        super()
        this.isFigure = true;
        this.isEnter = false;
        this.properties = {
            disabled: false,
            event: 'enter',
            nextTrigger: null,
            once: true,
            relMethod: '',
            relTarget: ''
        }
    }

    afterUpdate (c) {
        let {x, y, width, height, properties} = this;
        let { disabled } = properties;
        let { ref } = this.scene;
        let target = ref[this.properties.relTarget]

        if (target && !disabled) {
            if (this.isEnter) {
                this.trigger('Touch')
            }
            if (target.x > x && target.x < x + width && target.y > y && target.y < y + height) {
                if (this.isEnter === false) {
                    this.trigger('Enter')
                }
                this.isEnter = true;
            } else {
                if (this.isEnter === true) {
                    this.trigger('Leave')
                }
                this.isEnter = false;
            }
        }
        // c.strokeStyle = "#0f0"
        // c.strokeRect(x, y, width, height)
    }

    trigger (eventType) {
        if (this.properties['on' + eventType]) {
            eval('this.scene.ref.' + this.properties['on' + eventType])
        } else {
            return
        }

        if (this.properties.once) {
            this.properties.disabled = true
        }
    }
}