import { manager, scene } from "../../core";
let __frame = 0
class Bubble {
    constructor () {
        this.x = 0
        this.y = 0
        this.spriteIndex = manager.resources.sprites.bubble
        this.bubbleWidth = 32
        this.bubbleHeight = 26
        this.text = 'NoText'
        this.active = false
        this.afterAll = true
		scene.current.objects.push(this)
    }

    onSpawn () {
        let { properties } = this
        if (properties) {
            this.text = this.properties.text
            this.x = Math.round(this.x)
            this.y = Math.round(this.y)
        }
    }

    update (c) {
        if (!this.active) return
        let { texture } = this.spriteIndex
        __frame++
        c.save()
        c.translate(0, -this.bubbleHeight - 8 + this.amplitudeMovement())
        // Top Left Angle
        c.drawImage(texture, 0, 8, 8, 8, this.x, this.y, 8, 8)
        // Top Border
        c.drawImage(texture, 8, 8, 8, 8, this.x + 8, this.y, this.bubbleWidth - 16, 8)
        // Top Right Angle
        c.drawImage(texture, 16, 8, 8, 8, this.x + this.bubbleWidth - 8, this.y, 8, 8)
        // Right Border
        c.drawImage(texture, 16, 16, 8, 8, this.x + this.bubbleWidth - 8, this.y + 8, 8, this.bubbleHeight - 16)
        // Bottom Right Angle
        c.drawImage(texture, 16, 24, 8, 8, this.x + this.bubbleWidth - 8, this.y + this.bubbleHeight - 8, 8, 8)
        // Bottom Border
        c.drawImage(texture, 8, 24, 8, 8, this.x + 8, this.y + this.bubbleHeight - 8, this.bubbleWidth - 16, 8)
        // Bottom Left Angle 
        c.drawImage(texture, 0, 24, 8, 8, this.x, this.y + this.bubbleHeight - 8, 8, 8)
        // Left Border
        c.drawImage(texture, 0, 16, 8, 8, this.x, this.y + 8, 8, this.bubbleHeight - 16)

        // Background
        c.fillStyle = "#101420"
        c.fillRect(this.x + 7, this.y + 7, this.bubbleWidth - 15, this.bubbleHeight - 15);
 
        // Drawing Text
        c.fillStyle = "#fff"
        manager.resources.imageFont.main.context = c
        let metrics = manager.resources.imageFont.main.text(this.text, this.x + 8, this.y + 8)
        this.bubbleWidth = Math.round(metrics.width) + 20

        // Bubble Tail
        c.drawImage(texture, 0, 0, 32, 8, this.x, this.y + this.bubbleHeight - 2, 32, 8)
        c.restore()
    }

    amplitudeMovement () {
        return Math.round(Math.cos(__frame / 20) * 3)
    }
}

export default Bubble