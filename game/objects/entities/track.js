import Actor from "./actor";

class Track extends Actor {

    onSpawn () {
        let { path } = this;
        let copyPath = [];
        path.forEach((line, i) => {
            copyPath[i] = Object.assign({}, line)
            copyPath[i].x += this.x
            copyPath[i].y += this.y
        })

        this.path = copyPath
    }

    update (c) {
        // let { x, y, path } = this
        // if (path) {
        //     c.lineWidth = 1;
        //     c.beginPath()
        //     c.moveTo(path[0].x, path[0].y)
        //
        //     path.forEach(line => {
        //         c.lineTo(line.x, line.y)
        //     })
        //
        //     c.strokeStyle = "#ff0"
        //     c.stroke()
        // }
    }
}

export default Track