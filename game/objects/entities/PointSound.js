import { manager, scene } from "../../core";
import Actor from "./actor";

class PointSound extends  Actor{
    constructor() {
        super()
        this.properties = {
            loop: false,
            name: '',
            playOnSpawn: false,
            volume: 1,
            radius: 100
        }

        this.width = 0;
        this.height = 0;

        this.snd = null
        this.rect = {
            up: 0,
            down: 0,
            left: 0,
            right: 0
        }
    }

    onSpawn () {
        let { properties } = this;
        let { name, loop, volume, playOnSpawn } = properties;
        let { sounds } = manager.resources;
        this.snd = sounds[name].clone()
        this.snd.volume(volume);
        this.snd._loop = loop;

        if (playOnSpawn) {
            this.play()
        }
        let { x, y } = this
        let { map } = this.scene


        while (!map.solid.check_collision(x - this.rect.left, y) && this.rect.left < this.properties.radius) {
            this.rect.left++
        }
        while (!map.solid.check_collision(x + this.rect.right, y) && this.rect.right < this.properties.radius) {
            this.rect.right++
        }
        while (!map.solid.check_collision(x, y - this.rect.up) && this.rect.up < this.properties.radius) {
            this.rect.up++
        }
        while (!map.solid.check_collision(x, y + this.rect.down) && this.rect.down < this.properties.radius) {
            this.rect.down++
        }
    }

    play () {
        this.snd.play()
    }

    stop () {
        this.snd.stop()
    }

    afterUpdate (c) {
        super.afterUpdate()
        if (this.snd) {
            let {camera} = this.scene;
            let {canvas} = scene;
            let {x, y, width, height} = this;
            let { left, right, up, down } = this.rect;
            let {radius} = this.properties;
            let cx = camera.x + canvas.width / 2;
            let cy = camera.y + canvas.height / 2;

            let vol = (1 - Math.sqrt((x - cx) ** 2 + (y - cy) ** 2) / radius)
            let pan = (x - cx) / canvas.width;
            pan = Math.max(-1, Math.min(pan, 1));
            vol = Math.max(0, Math.min(vol, 1));

            this.snd.stereo(pan)
            if (cx > x - left && cx < x + right && cy > y - up && cy < y + down) {
                this.snd.volume(vol)
            } else {
                this.snd.volume(0)
            }


        }
    }

    beforeDestroy () {
        this.snd.stop()
    }
}

export default PointSound