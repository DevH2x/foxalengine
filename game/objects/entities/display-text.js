import state from "../../state";
import animation from "../../animation";

class DisplayText {
    constructor (text) {
        this.text = text
        state.guiObjects.push(this)
        this.interval = 0.3
        this.time = this.interval
        this.letter = 0
        this.active = true
        this.alpha = 1
        this.holdTime = 100
    }

    update (c) {
        let width
        c.font = "24pt Main"
        c.textAlign = "left"
        c.textBaseline = "top"
        width = c.measureText(this.text).width
        c.globalAlpha = this.alpha
        c.fillStyle = "#0f0"
        c.fillText(this.text.substr(0, this.letter), Math.round(c.canvas.width / 2 - width / 2) + 0.5, Math.round(c.canvas.height / 2) - 12)
        c.fillStyle = "#fff"
        c.fillText(this.text.substr(0, this.letter - 2), Math.round(c.canvas.width / 2 - width / 2) + 0.5, Math.round(c.canvas.height / 2) - 12)
        c.globalAlpha = 1

        if (--this.time < 0) {
            this.time = this.interval
            this.letter++
        }

        if (this.letter > this.text.length + this.holdTime && this.active) {
            this.active = false
            animation(percent => {
                this.alpha = 1 - percent
            }, 500)
                .then(() => {
                    state.guiObjects.splice(state.guiObjects.indexOf(this), 1);
                })
        }
    }
}

export default DisplayText