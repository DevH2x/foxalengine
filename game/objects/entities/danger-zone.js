import Area from "./area";
import state from "../../state";
import AcidDrop from "../acid-drop";
class DangerZone extends Area {
    constructor () {
        super()
        this.__debugColor = "#5af"
        this.__frame = Math.round(Math.random() * 120)
    }

    update (c) {
        super.update(c)
        if (state.debug) {
            c.fillStyle = this.__debugColor
            c.fillText("DangerZoneType: " + this.properties.type, this.x + 16, this.y + 16)
        }
        if (this.properties.type && ++this.__frame % 120 === 0) {
            switch (this.properties.type) {
                case 'ACID':
                    let drop = new AcidDrop()
                    drop.x = this.x + this.width * Math.random()
                    drop.y = this.y + 24
                break;

                default: break;
            }
        }

        if (this.playerInArea) {
            this.scene.ref.plr.damage()
        }
    }
}

export default DangerZone