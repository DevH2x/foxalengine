import Area from "./area";
import io from "../../io";

class Ladder extends Area {
    constructor () {
        super()
        this.__debugColor = "#5af"
    }

    update (c) {
        super.update(c)
        if (this.playerInArea) {
            this.scene.ref.plr.ladderSpeed.y = io.keyboard_check(io.KEY_DOWN) - io.keyboard_check(io.KEY_UP)
            this.scene.ref.plr.ladderSpeed.x = io.keyboard_check(io.KEY_RIGHT) - io.keyboard_check(io.KEY_LEFT)
            this.scene.ref.plr.ladder = true
        }
    }
}

export default Ladder