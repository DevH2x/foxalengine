import Actor from './entities/actor.js'
import { manager } from '../core.js'

export default class Vent extends Actor {
    constructor (){
        super();
        this.sprite = manager.resources.sprites.vent.clone();
        this.rotate_speed = 5 + Math.random() * 10
    }


    beforeUpdate () {
        this.sprite.angle += this.rotate_speed
    }
}
