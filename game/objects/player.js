import Actor from './entities/actor.js'
import { io, manager } from '../core.js'
import state from '../state'
import CPointLight from './entities/c-point-light.js';
import animation, { ease } from '../animation.js';

export default class Player extends Actor {
	constructor () {
		super();
        
        // Sprites control
        this.sprite_list                        = {
			'run':                              this.SPRITES.player_run,
			'idle':                             this.SPRITES.player_idle,
			'jump':                             this.SPRITES.player_jump,
			'fall':                             this.SPRITES.player_fall,
			'impact':                           this.SPRITES.player_impact,
            'attack':                           this.SPRITES.player_attack,
            'ladder':                           this.SPRITES.player_ladder
		};
		this.sprite_current                     = 'idle';
		this.sprite_scale                       = { x: 1, y: 1 }
        this.sprite                             = this.sprite_list[this.sprite_current];
        
        // Flags
		this.ground                             = false;
        this.impact                             = false;
        this.controlDisabled                    = false;

        // Physics
		this.friction.x                         = 0.8;
		this.acceleration                       = 0.2;
		this.accelerationRun                    = .7;
		this.accelerationFly                    = 0.1;
        this.accelerationImpact                 = 0.1;
        this.ladderSpeed                        = { x: 0, y: 0 };
        this.Speed                              = { x: 0, y: 0 };
        this.movementDirection                  = 0

        // Misc
        this.savedPoint                         = { x: 0, y: 0 }

        // Lighting
        this.light                              = new CPointLight()
        this.lightSavePoint                     = new CPointLight()
        this.lightSavePoint.properties.color    = "#ff55aaff"
        this.lightSavePoint.properties.scale    = 5
        this.light.properties.enabled           = false
	}

    impactStart () {
        this.sprite_current                                     = 'impact';
		this.impact                                             = true;
		this.sprite_list[this.sprite_current ].frame_current    = 0;
        this.SOUNDS.player_impact.play()
	}

	onSpawn () {
        this.x += 8
        this.y += 8
        setTimeout(() => this.scene.objects.swap(this.id, 100), 0)
        this.save()
    }

    controls (c) {
        let acceleration    = (io.keyboard_check(io.KEY_RIGHT) - io.keyboard_check(io.KEY_LEFT)) * this.acceleration
        // Acceleration
        this.Speed.x += acceleration;

        // Gravity
        this.Speed.y += Math.sin(Math.PI / 2) * state.gravity

        if (this.ladder) {
            this.friction.x = 0.8                           // Ladder Movement
        } else {
            if (this.ground) {                              // Ground Movement
                this.friction.x = 0.8                       
                this.acceleration = this.accelerationRun
            } else {                                        // Air Movement
                this.friction.x = 0.95
                this.acceleration = this.accelerationFly;
            }
        }


        io.keyboard_once(' ', () => {
            if (this.ground) {
                this.y -= 8
                this.Speed.y = -3
                this.SOUNDS.player_jump.play()
            }
        })

        this.ground = false;
    }

    applyPhysics (c) {
        let positiveMovementDirection = Math.abs(this.movementDirection)
        if (this.ground) {
            this.movementDirection = this.calculateMovementDirection()
            if (positiveMovementDirection > Math.PI / 4) {
                this.movementDirection = 0
            }
        } else {
            this.movementDirection = 0
        }

        // Friction power
        this.Speed.x *= this.friction.x

        // Horizontal Movement
        this.x += Math.cos(this.movementDirection) * this.Speed.x
        this.y += Math.sin(this.movementDirection) * this.Speed.x

        // Vertical Movement
        this.x += Math.cos(Math.PI / 2) * this.Speed.y
        this.y += Math.sin(Math.PI / 2) * this.Speed.y
    }

    // TODO: Slash ground collision
    collision () {
        let { x, y } = this;
        let { map } = this.scene;
        let bbox = {
            width: 8,
            height: 12
        }
        if (map) {
        	// Vertical
            let sign = Math.sign(this.Speed.y);
            let offset = Math.round(bbox.height / 2 - 1) * sign;
            for (let i = -bbox.width / 2; i < bbox.width / 2; i++) {
                if (this.check_collision(this.x + i, this.y + offset + this.Speed.y)) {
                    while(!this.check_collision(this.x + i, this.y + offset + sign)) {
                        this.y += sign
                    }
                    this.Speed.y = 0
                    break;
                }
            } 
            
            // Horizontal
            sign = Math.sign(this.Speed.x);
            offset = bbox.width / 2 * sign;
            for (let i = -Math.ceil(bbox.height / 2); i < Math.ceil(bbox.height / 2); i++) {
                if (this.check_collision(this.x + offset + this.Speed.x, this.y + i)) {
                    while(!this.check_collision(this.x + offset + sign, this.y + i)) {
                        this.x += sign
                    }
                    this.Speed.x = 0
                    break;
                }
            } 

            this.ground = this.check_collision(x, y + 10) || this.check_collision(x + 3, y + 10) || this.check_collision(x - 3, y + 10);

			if (this.ground) {
			    this.attacked = false;
            }
        }
    }

    animation () {
	    let { sounds } = manager.resources;
        let speed = Math.abs(this.Speed.x);
        if (speed > 0) {
            this.sprite_scale.x = Math.sign(this.Speed.x);
        }

        if (!this.ladder) {
            if (this.ground) {
                if (this.impact) {
                    this.acceleration = 0.2;
                    this.sprite_list['impact'].frame_speed = 0.2;
                    if (this.sprite_list['impact'].frame_current >= 6) {
                        this.impact = false;
                    }
                } else {
                    this.acceleration = this.accelerationRun;
                    if (Math.abs(this.Speed.x) > 0.5) {
                        this.sprite_current = 'run';
                        if (Math.floor(this.sprite.frame_current) === 7 || Math.floor(this.sprite.frame_current) === 0) {
                            sounds.player_impact.play()
                            this.sprite.frame_current++
                        }
                    } else {
                        this.sprite_current = 'idle';
                    }
                }
                this.sprite.angle = 0//Math.sign(this.Speed.x) * this.calculateMovementDirection() * 180 / Math.PI
            }else{
                this.impact = false
                if (!this.attack) {
                    if (this.Speed.y > 0) {
                        this.sprite_current = 'fall';
                    } else {
                        this.sprite_current = 'jump';
                        this.sprite.frame_speed = 0.2
                    }
                    this.sprite.angle = -this.Speed.y * Math.abs(this.Speed.x) * 2
                } else {
                    this.sprite_current = 'attack'
                }
            }
            if (!this.impact) {
                if (this.sprite_current === 'run') {
                    this.sprite.frame_speed = Math.abs(this.Speed.x) / 8
                } else {
                    this.sprite.frame_speed = 0.4
                }
            }
        } else {
            if (!this.ground || Math.abs(this.ladderSpeed.y) > 0) {
                this.sprite_current = 'ladder'
            }
            this.Speed.y = 0;
            this.sprite.frame_speed = Math.abs(this.ladderSpeed.y) * 0.15;
            this.Speed.x = this.ladderSpeed.x
            if (Math.round(this.sprite.frame_current) === 0 || Math.round(this.sprite.frame_current) === 3) {
                if (!this.check_collision(this.x, this.y + this.ladderSpeed.y * 16)) {
                    this.Speed.y += this.ladderSpeed.y * 10
                    this.sprite.frame_current++
                    sounds.player_impact.play()
                }
            }
            this.impact = false
        }
		
		this.sprite = this.sprite_list[this.sprite_current];
		
        this.sprite.offset = {
            x: 8,
            y: 10
        }
        this.sprite.scale = this.sprite_scale;
    }

	update (c) {
        super.update(c)
        
        if (state.controls && this.scene.spectateTo === this.name) {
            this.controls(c);
        }
        
        if (!state.playerRespawn) {
            this.collision(c);
        }

        this.animation();
        this.drawSavePoint(c);
        if (this.light.properties.enabled) {
            this.light.x = this.x
            this.light.y = this.y
        }
    }
    
    save () {
        this.savedPoint.x = Math.round(this.x)
        this.savedPoint.y = Math.round(this.y)
    }

    drawSavePoint (c) {
        let { sprites } = manager.resources;
        sprites.player_savepoint.draw(this.savedPoint.x, this.savedPoint.y)
        c.filter = 'blur(2px)'
        c.globalCompositeOperation = "color-dodge"
        sprites.player_savepoint.draw(this.savedPoint.x, this.savedPoint.y)
        c.globalCompositeOperation = "source-over"
        c.filter = 'blur(0)'
        this.lightSavePoint.x = this.savedPoint.x
        this.lightSavePoint.y = this.savedPoint.y
    }

    calculateMovementDirection (c) {
        let steps = 30
        let p1 = { x: -4, y: -8}
        let p2 = { x: 4, y: -8}
        for (let i = 0; i < steps; i++) {
            if (this.check_collision(this.x + p1.x, this.y + p1.y)) {
                break;
            }
            p1.y++
        }
        for (let i = 0; i < steps; i++) {
            if (this.check_collision(this.x + p2.x, this.y + p2.y)) {
                break;
            }
            p2.y++
        }
        return Math.atan2((p2.y - p1.y), (p2.x - p1.x))
    }

    damage () {
        if (state.playerRespawn) return
        this.light.properties.enabled = true
        let dx = this.x - this.savedPoint.x
        let dy = this.y - this.savedPoint.y

        state.playerRespawn = true
        state.shakeStart(32, 1, 1, 300, 'OutCubic')

        manager.resources.sounds.player_restore.play()
        manager.resources.sounds.nt_lose.play()

        animation(percent => {
            let invertedPercent = ease.easeInCubic(1 - percent);
            this.x = this.savedPoint.x + dx * invertedPercent;
            this.y = this.savedPoint.y + dy * invertedPercent;
            this.Speed.y = 0
        }, 1000)
        .then(() => {
            this.light.properties.enabled = false
            this.x = this.savedPoint.x
            this.y = this.savedPoint.y
            state.playerRespawn = false
        })
    }

	afterUpdate (c) {
        super.afterUpdate(c);
        this.ladder = false
        this.applyPhysics(c)
	}
}
