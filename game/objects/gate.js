import Actor from "./entities/actor.js";
import { io } from "../core.js";
import TouchLocker from "./entities/no-touch-locker.js";
import state from "../state.js";
import animation, { ease } from "../animation.js";

class Gate extends Actor {
    constructor () {
        super()
        this.solidArea = null
        this.game = null
        this.opened = false
        this.startHeight = 0
        this.work = false
    }

    onSpawn () {
        this.solidArea = this.scene.createSolidArea(this.x, this.y, this.width, this.height)
        this.startHeight = this.height
    }

    update (c) {
        if (this.game || this.opened) return
        let { plr } = this.scene.ref
        let plrDistance = Math.abs(this.x + this.width / 2 - plr.x)
        if (plr.y > this.y && plr.y < this.y + this.height && plrDistance < this.width / 2 + 16) {
            c.fillStyle = "#000"
            c.fillRect(this.x, this.y, this.width, this.height)
            
            if (!this.work) {
                io.keyboard_once('X', () => {
                    let __height = this.height
                    let __heightTarget = __height - 8
                    this.opened = true
                    this.work = true
                    this.SOUNDS.gate_open.play()
                    this.SOUNDS.click.play()
                    animation(percent => {
                        this.height = __height - __heightTarget * percent
                    }, 500)
                })
            }
        }
    }

    startGame () {
        state.game = new TouchLocker(2, 8, 32)
        state.game.onLose = () => {
            state.game = null
        }

        state.game.onUnlock = () => {
            this.opened = true;
            state.game = null
        }
    }

    beforeUpdate (c) {
        c.fillStyle = "#000"
        c.fillRect(this.x, this.y, this.width, this.height)
        this.solidArea.h = this.height

        if (this.game) {
            this.game.update(c)
        }
    }
}

export default Gate