import Actor from "./entities/actor";
import {manager} from "../core";
import CPointLight from "./entities/c-point-light";

class Explode extends Actor {
    constructor () {
        super()
        this.sprite = manager.resources.sprites.explosion.clone()
        this.light = new CPointLight()
        this.light.properties.color = "#ffffff00"
        this.light.properties.scale = 5;
        this.light.render()
        this.sprite.frame_speed = 0.2
        manager.resources.sounds.explosion.play()
    }

    update () {
        this.light.x = this.x;
        this.light.y = this.y;
        if (Math.round(this.sprite.frame_current) >= this.sprite.frame_count) {
            this.light.destroy()
            this.scene.objects.destroy(this.id)
        }
    }
}

export default Explode