import Item from "./entities/item";
import state from "../state";

class ItemPDA extends Item {
    constructor () {
        super()
        this._sprite = this.SPRITES.item_pda.clone()
        this.canvas = document.createElement('canvas')
        this.context = this.canvas.getContext('2d')
        this.canvas.width = 300
        this.canvas.height = 300
        this.opened = false
        this.height = 0
    }

    render () {
        let chat = this.properties.messages.split('\n')
        let fs = this.FONT.main.size
        let y = fs
        let c = this.context
        let canvas = this.canvas
        console.log(chat)
        c.fillStyle = "#000"
        c.globalAlpha = 0.5
        c.fillRect(0, 0, canvas.width, canvas.height)
        c.globalAlpha = 1
        chat.forEach((message, i) => {
            if (!message) return
            let side = i % 2 === 0
            let ms = message.split('#')
            let h = this.FONT.main.size * ms.length + this.FONT.main.size * 2
            let x = side ? canvas.width - 200 - 16 : 16
            c.fillStyle = side ? "#5af" : "#30a"
            c.fillRect(x, y, 200, h)
            
            ms.forEach((line, j) => {
                // c.fillText(line, x + 8, y + fs * .5 + fs * j)
                this.FONT.main.context = c
                this.FONT.main.text(line, x + 8, Math.round(y + this.FONT.main.size * j) + this.FONT.main.size)
            })

            y += h + fs
        })

        this.height = y
    }

    onUse () {
        this.opened = !this.opened
        state.controls = !this.opened
        if (this.opened) {
            this.render()
            this.scene.ref.plr.x = this.x
            this.scene.ref.plr.y = this.y
        }
    }

    renderGui (c) {
        if (this.opened) {
            let x = Math.round(c.canvas.width / 2 - this.canvas.width / 2)
            let y = Math.round(c.canvas.height / 2 - this.height / 2)
            c.filter = "blur(4px)"
            c.drawImage(c.canvas, x, y, this.canvas.width, this.canvas.height, x, y, this.canvas.width, this.canvas.height)
            c.filter = 'none'
            c.drawImage(this.canvas, x, y)
            c.strokeStyle = "#fff"
            c.lineWidth = 1;
            c.strokeRect(c.canvas.width / 2 - this.canvas.width / 2, c.canvas.height / 2 - this.height / 2, this.canvas.width, this.height)
        }
    }
}

export default ItemPDA