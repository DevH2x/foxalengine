import Actor from './entities/actor.js'
import { io, manager } from '../core.js'



export default class Door extends Actor {

    constructor () {
        let { sprites } = manager.resources;
        super()
        this.sprite = sprites.door_unclosed.clone();
        this.sprite.frame_speed = 0;
        this.opened = false;
        this.index = 1
    }

    afterUpdate () {
        let { sounds } = manager.resources
        let { ref } = this.scene;
        let { plr } = ref;
        let { moveToRel } = this.properties
        let target = ref[moveToRel];
        if (plr) {
            if (plr.x > this.x && plr.x < this.x + this.sprite.frame_width &&
                plr.y > this.y && plr.y < this.y + this.sprite.frame_height && !plr.controlDisabled) {
                io.keyboard_once('X', () => {
                    this.opened = true;
                    if (target) {
                        target.opened = true;
                        sounds.gate_open.play()
                    }
                    this.SOUNDS.click.play()
                    io.gamepad_vibrate(0, 1, 600)
                    plr.controlDisabled = true;
                    setTimeout(() => {
                        plr.controlDisabled = false;
                        let offsetX = this.x - plr.x;
                        let offsetY = this.y - plr.y;

                        plr.x = target.x - offsetX;
                        plr.y = target.y - offsetY;

                        this.opened = false;
                        setTimeout(() => {
                            if (target) {
                                target.opened = false;
                                sounds.gate_close.play()
                            }
                        }, 400)
                    }, 500)
                })
            }

            if (this.opened) {
                if (this.sprite.frame_current < this.sprite.frame_count - 0.2) {
                    this.sprite.frame_current += 0.2
                } else {
                    this.sprite.frame_current = this.sprite.frame_count
                }
            } else {
                if (this.sprite.frame_current > 0) {
                    this.sprite.frame_current -= 0.2
                } else {
                    this.sprite.frame_current = 0
                }
            }
        }
        super.afterUpdate()
    }
}
