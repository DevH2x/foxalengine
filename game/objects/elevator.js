import Actor from "./entities/actor";
import { manager } from "../core";

class Elevator extends Actor {
    constructor() {
        super();
        this.properties = {
            activator: null,
            startpoint: 0,
            track: null
        }
        this.sprite = manager.resources.sprites.elevator.clone();
        this.solidArea = null;

        // Moving props
        this.isMoving = false;
        this._speed = 2;
        this.currentPoint = 0;
        this.direction = 'forward';
        this.track = null;
    }

    afterUpdate (c) {
        super.afterUpdate(c)
        if (this.solidArea) {
            this.solidArea.x = this.x;
            this.solidArea.y = this.y;
        }
    }

    onSpawn () {
        let { track, speed } = this.properties
        this.solidArea = this.scene.createSolidArea(this.x, this.y, 48, 16)

        setTimeout(() => {
            this.track = this.scene.ref[track].path
        }, 1)

        this._speed = speed || this._speed;
    }


    beforeUpdate (c) {
        if (this.track) {
            let {x, y, track, currentPoint, _speed, direction} = this;
            if (this.isMoving) {
                let dx = track[currentPoint].x - x;
                let dy = track[currentPoint].y - y;
                let dis = Math.sqrt(dx ** 2 + dy ** 2);
                let ang = Math.atan2(dy, dx);

                if (dis < _speed) {
                    switch (direction) {
                        case 'forward':
                            if (currentPoint < track.length - 1) {
                                this.currentPoint++
                            } else {
                                this.isMoving = false;

                                if (track[currentPoint].x === track[0].x && track[currentPoint].y === track[0].y) {
                                    this.currentPoint = 0;
                                }
                            }
                            break;

                        case 'backward':
                            if (currentPoint > 0) {
                                this.currentPoint--
                            } else {
                                this.isMoving = false;
                            }
                            break;
                    }

                    this.speed.x = 0;
                    this.speed.y = 0;
                    this.x = track[currentPoint].x;
                    this.y = track[currentPoint].y;
                } else {
                    this.speed.x = Math.cos(ang) * _speed;
                    this.speed.y = Math.sin(ang) * _speed;
                }
                for (let id in this.solidArea.objects) {
                    let obj = this.solidArea.objects[id];
                    obj.x += this.speed.x;
                    obj.y += this.speed.y;
                }
            } else {
                this.speed.x = 0;
                this.speed.y = 0;
                this.x = track[currentPoint].x;
                this.y = track[currentPoint].y;
            }


        }
    }

    startForward () {
        this.direction = 'forward';
        this.isMoving = true;
    }

    startBackward () {
        this.direction = 'backward'
        this.isMoving = true
    }
}

export default Elevator