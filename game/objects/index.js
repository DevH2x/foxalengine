// Logic & Environment
import Trigger from "./entities/trigger";
import BlackZone from "./entities/blackzone";
import Reflection from "./entities/reflection";
import Camera from "./entities/camera";
import DangerZone from './entities/danger-zone';
import Ladder from './entities/ladder';
import CPointLight from "./entities/c-point-light";
import SaveZone from './entities/save-zone';
import ParticleType from './entities/particle-type';
import PointSound from "./entities/PointSound";
import Track from "./entities/track";
import Bubble from './entities/bubble';
import Actor from './entities/actor';
import MusicZone from "./entities/music-zone";

// Gaming Elements
import Player from './player';
import Door from './door'
import Vent from './vent'
import Elevator from "./elevator";
import Gate from './gate';
import Mouse from "./mouse";
import ItemPDA from "./item-pda";


let objectsList = { 
    Player, 
    Actor, 
    Door, 
    Vent, 
    ParticleType, 
    Trigger, 
    CPointLight, 
    BlackZone, 
    PointSound, 
    Reflection, 
    Track, 
    Camera,
    Elevator, 
    Gate, 
    Bubble, 
    DangerZone,
    SaveZone,
    Ladder,
    MusicZone,
    Mouse,
    ItemPDA
};


export { Actor, Player }
export { objectsList }

export default Actor
