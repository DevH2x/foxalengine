let createObjectData = () => {
    return {
        list: { },
        listAfterAll: { },
        length: 0,
        push ( obj ) {
            let id = 0
            while(this[obj.afterAll ? 'listAfterAll' : 'list'][id]) {
                id++
            }
            obj.id = id
            this[obj.afterAll ? 'listAfterAll' : 'list'][id] = obj;
        },
    
        swap (id1, id2) {
            this.list['temp'] = this.list[id1]
            this.list[id1] = this.list[id2]
            this.list[id2] = this.list['temp'];
            if (this.list[id2]) {
                this.list[id2].id = id1
            }
            if (this.list[id1]) {
                this.list[id1].id = id2
            }
            delete this.list['temp'];
        },
    
        destroy (id) {
            if (this.list[id]) {
                if (this.list[id].beforeDestroy)
                    this.list[id].beforeDestroy();
            }
            delete this.list[id];
        },
    
        forEach (handler) {
            for (let i in this.list) {
                handler(this.list[i])
            }
        },
    
        forEachAfter (handler) {
            for (let i in this.listAfterAll) {
                handler(this.listAfterAll[i])
            }
        },
    
        getByType (type) {
            let array = []
            for (let key in this.list) {
                if (!this.list[key]) continue
                if (this.list[key].type === type) {
                    array.push(this.list[key])
                }
            }
            return array
        },
    
        destroyAll () {
            for (let i in this.list) {
                this.destroy(i)
            }
    
            for (let i in this.listAfterAll) {
                if (this.listAfterAll[i].beforeDestroy)
                    this.listAfterAll[i].beforeDestroy()
                delete this.listAfterAll[i]
            }
        },

        update (context) {
            this.forEach(object => {
                if (object) {
                    if (object.beforeUpdate) object.beforeUpdate(context);
                    if (object.update) object.update(context);
                    if (object.afterUpdate) object.afterUpdate(context);
                }
            })
        },

        renderGui (context) {
            this.forEach(object => {
                if (object) {
                    if (object.renderGui) object.renderGui(context)
                }
            })
        },

        afterUpdate (context) {
            this.forEachAfter(object => {
                if (object.beforeUpdate) object.beforeUpdate(context);
                if (object.update) object.update(context);
                if (object.afterUpdate) object.afterUpdate(context);
            })
        }
    };
}

export default createObjectData