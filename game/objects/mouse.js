import Actor from "./entities/actor";
import state from "../state";

class Mouse extends Actor {
    constructor () {
        super()
        this.sprites            = {
                                run: this.SPRITES.mouse_run.clone(),
                                idle: this.SPRITES.mouse_idle.clone()
        }
        this.spriteCurrent      = 'idle'
    
        this.frame              = 0
        this.framesPerStep      = 60
        this.framesMaxInterval  = 100
        this.movementDirection  = 1     // 1 - Right, -1 - Left
        this.isMovement         = false
        this.acceleration       = 0.4
        this.friction.x         = 0.8
    }

    collision () {
        let speed, speedSign
        let { x, y } = this
        // Vertical
        speed = Math.abs(this.speed.y)
        speedSign = Math.sign(this.speed.y)

        for (let i = 0; i < speed + 1; i++) {
            if (this.check_collision(x, y + 4 * speedSign + i * speedSign)) {
                this.y += i
                this.speed.y = 0
                break;
            }
        }

        // Horizontal
        speed = Math.abs(this.speed.x)
        speedSign = Math.sign(this.speed.x)
        for (let i = 0; i < speed + 1; i++) {
            if (this.check_collision(x + 12 * speedSign + i * speedSign, y)) {
                this.x += i
                this.speed.x = 0
                break;
            }
        }
    }

    animation () {
        let positiveSpeedX = Math.abs(this.speed.x)
        this.sprite = this.sprites[this.spriteCurrent]
        if (positiveSpeedX > 0.1) {
            this.spriteCurrent = 'run'
            this.sprite.frame_speed = positiveSpeedX / 4
        } else {
            this.spriteCurrent = 'idle'
            this.sprite.frame_speed = 0
        }
        this.sprite.scale.x = Math.sign(this.speed.x) || this.sprite.scale.x
    }

    randomMovement () {
        if (++this.frame > this.framesPerStep) {
            this.movementDirection = Math.sign(-1 + Math.random() * 2)
            this.framesPerStep = Math.random() * this.framesMaxInterval
            this.isMovement = !this.isMovement
            this.frame = 0
        }

        if (this.isMovement) {

            if (!this.check_collision (this.x + 12 * this.movementDirection, this.y + 8)) {
                this.movementDirection = -this.movementDirection
            }
            this.speed.x += this.acceleration * this.movementDirection
        }
    }

    moveToPlayer () {
        let { plr } = this.scene.ref
        if (plr) {
            this.movementDirection = Math.sign(plr.x - this.x)
            this.speed.x += this.acceleration * this.movementDirection
        }
    }

    movement () {
        // if (this.playerDistance > 200) {
        //     this.randomMovement()
        // } else {
        //     this.moveToPlayer()
        // }
        
        this.randomMovement()
    }

    beforeUpdate (c) {
        this.speed.y += state.gravity
    }

    update (c) {
        super.update(c)
        this.movement()
        this.collision()
        this.animation()
    }
}

export default Mouse