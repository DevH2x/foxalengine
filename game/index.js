import Core from './core';
import Resources from './resources'
import './scenes';
import IOGamepad from  '../core/gamepad.io.class';

let e = new IOGamepad();
Resources(Core.manager);

export default () => {
    Core.init(e=>{
        Core.scene.load('engine');
    })
}
