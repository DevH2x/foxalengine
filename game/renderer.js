import Engine from '../core'
import commandPanel from './control-panel';
import manager from './manager'
import { scene } from './core';
// Creating Renderer
let renderer = new Engine.Renderer(() => {
    let c = renderer.context;
    let canvas = renderer.canvas;
    if (manager.files_total <= manager.files_loaded) {
        scene.render()
    } else {
        c.fillStyle = "#000"
        c.fillRect(0, 0, canvas.width, canvas.height)
        c.lineWidth = 6
        c.strokeStyle = "#333"
        c.beginPath()
        c.moveTo(canvas.width / 2 - 64, canvas.height / 2)
        c.lineTo(canvas.width / 2 + 64, canvas.height / 2)
        c.stroke()

        c.strokeStyle = "#f80"
        c.beginPath()
        c.moveTo(canvas.width / 2 - 64, canvas.height / 2)
        c.lineTo(canvas.width / 2 - 64 + 128 * (manager.files_loaded / manager.files_total), canvas.height / 2)
        c.stroke()

        c.fillStyle = "#f80"
        c.textAlign = 'center'
        c.textBaseline = 'center'
        c.font = "32px Main"
        c.fillText('Loading...', canvas.width / 2, canvas.height / 2 - 16)
    }

    commandPanel.render(renderer.context)
    
    // c.fillStyle = "#fff"
    // c.font = "24pt Main"
    // c.fillText("Stifler: Development Version", 16.25, 0)
});
renderer.canvas.style.imageRendering = 'pixelated';
renderer.canvas.width = 640;
renderer.canvas.height = 320;
renderer.scale = 1;


export default renderer
