import animation, { ease } from "./animation";
export default {
    gravity: 0.2,
    game: null,
    debug: false,
    saveZone: false,
    saveZoneObject: false,
    playerRespawn: false,
    guiObjects: [],
    shake: 0,
    shakeTimeout: false,
    shakeAnimation: false,
    currentMusic: null,
    currentZone: null,
    controls: true,
    tick () {
        this.saveZone = false
    },
    shakeStart(amplitude, duration, upDuration, downDuration, easeFunction) {
        let shakeSnapshot = this.shake
        let shakeTarget = amplitude - this.shake
        easeFunction = ease['ease' + (easeFunction || 'InOutCubic')]
        if (this.shakeTimeout) {
            clearTimeout(this.shakeTimeout)
        }
        animation((percent) => {
            this.shake = shakeSnapshot + easeFunction(percent) * shakeTarget
        }, upDuration)
        .then(() => {
            this.shakeTimeout = setTimeout(() => {
                animation(percent => {
                    this.shake = shakeSnapshot + easeFunction(1 - percent) * shakeTarget
                }, downDuration)
            }, duration)
        })
    }
}