export default (data, resolve) => {
    fetch(data.file)
    .then(type => type.blob())
    .then(blob => {
        let fr = new FileReader()
        fr.onloadend = event => {
            let base64 = event.target.result;

            document.head.innerHTML += `<style>
                @font-face {
                    font-family: "${data.name}";
                    src: url(${base64});
                }
                </style>
            `
            resolve()
        }
        fr.readAsDataURL(blob)
    })
}