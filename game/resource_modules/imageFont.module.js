import { Font } from "../../core";

export default (data, resolve) => {
    let font = new Font(data.file, data.size)
    font.letterSpacing = data.letterSpacing || 0
    console.log(font)
    font.onLoad = e => {
        resolve(font)
    }
}