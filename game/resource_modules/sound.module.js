export default (data, resolve) => {
    let snd;
    let { Howl } = window;
    if (Howl) {
        let options = Object.assign({}, data);
        options['src'] = [options.file];
        options.onload = () => {
            resolve(snd)
        }
        snd = new Howl(options)
        snd.clone = function () {
            return new Howl(options)
        }
    }
}