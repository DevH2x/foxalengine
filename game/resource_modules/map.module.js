import Tilemap from '../../core/tilemap.class';

export default (data, resolve) => {
    let tilemap = new Tilemap(data.file, ()=>{
        tilemap.solid = tilemap.prerenderSolidMask('$solid');
        resolve(tilemap);
    })
}