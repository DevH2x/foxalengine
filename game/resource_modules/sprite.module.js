import Sprite from '../../core/sprite.class'
import { renderer } from '../core';

export default (data, resolve) => {
    let texture = new Image();
    texture.src = data.file;
    texture.onload = () => {
        let sprite = new Sprite(texture);
        sprite.context = renderer.context;
        sprite.set_frame_size(data.w || texture.width, data.h || texture.height);

        if (data.o) {
            sprite.offset.x = data.o.x || 0;
            sprite.offset.y = data.o.y || 0;
        }
        resolve(sprite);
    }
}
