const path = require('path');

module.exports = {
    entry: './index.js',
    output:{
        filename: 'game.js',
        path: path.resolve(__dirname, 'production')
    },
    mode: 'development',
    module: {
        rules: [
            {
                test: /.xml$/,
                use: 'raw-loader'
            }
        ]
    },
    devServer: {
        contentBase: './production'
    }
};